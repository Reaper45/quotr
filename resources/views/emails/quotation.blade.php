<!doctype html>
<html>
<head>

<style>

    body {
        color: #636b6f;
        font-weight: 100;
        height: 100vh;
        font-family: Raleway,sans-serif;
        font-size: 14px;
        line-height: 1.6;
        background-color: #f5f8fa;
    }
    .navbar{
        margin-bottom: 0;
    }
    .account-container{
        height: 50px;
        background: transparent linear-gradient(100deg, rgb(16, 101, 140), rgb(38, 180, 236)) repeat scroll 0% 0%;
        width: 100%;
        padding: 0;
    }

    .breadcrumb{
        background-color: #fff;
    }
    .fa-2x {
        font-size: 2em;
    }
    .fa-3x {
        font-size: 3em;
    }
    .app-footer {
    color: #3b5998;
    }
    .breadcrumb > li + li::before{
        content: '';
    }
    .breadcrumb{
        border-radius: 25px;
        padding: 8px 15px;
        margin-bottom: 22px;
        list-style: none;
        background-color: #fff;
    }
    .breadcrumb > li {
        display: inline-block;
    }
    .breadcrumb > .active {
        color: #777;
    }
    a {
        color: #3097d1;
        text-decoration: none;
        background-color: transparent;
    }

    .pull-right {
        float: right !important;
    }

    .app-footer {
        position: absolute;
        background: transparent;
        width: 100%;
        color: #fff;
        border-bottom: solid 5px rgb(16, 101, 140);
        color: #3b5998;
        line-height: 5;
        margin-bottom: 0px;
    }
    p {
        margin: 0 0 11px;
    }
    .row::after, .panel-body::after{
        clear: both;
    }
    *, *::after, *::before {
        box-sizing: border-box;
    }

    .text-center{
        text-align: center;
    }
    .table-bordered > tbody > tr > th {
        border: 1px solid #ddd;
    }
    .table-bordered > tbody > tr > td {
        border: 1px solid #ddd;
    }
    .table-bordered {
        border: 1px solid #ddd;
    }
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }
    table {
        background-color: transparent;
    }
    table {
        border-spacing: 0;
        border-collapse: collapse;
    }
    .table > tbody > tr > th{
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

    th {
        text-align: left;
    }
    .table > tbody > tr > td{
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }


</style>
</head>
<body>
    <div class="account-container"></div>
    <div class="container" style="padding: 20px 10px; ">
        <div>
            <span class="fa-3x">Quotation</span>
        </div>
        <div>
            <span class="fa-2x">No. 0001</span>
            <span style="margin-left: 20px;">{{ date('F j, Y, g:i a', time()) }}</span>
            <span class="pull-right fa-2x">Received From: &nbsp; {{ $cart['from']->name}}</span>

        </div>

        <ol class="breadcrumb">
            <li>Client: </li>
            <li class="active"><a href="#">{{ $cart['to']->name }}</a></li>
        </ol>
        <ol class="breadcrumb">
            <li>City: </li>
            <li class="active"><a href="#">{{ $cart['to']->location }}</a></li>
        </ol>

        <div class="table-responsive">
            <table class="table table-bordered" style="width: 100%;">
                <tr >
                    <th >Qty</th>
                    <th >Products/Service</th>
                    <th >Item Name</th>
                    <th >Unit Price</th>
                    <th >Total</th>
                </li>
                @if(Session::has('mail-cart') && $items =  Session::get('mail-cart')['products'])
                    @foreach($items as $key => $value)
                        @if( $key != 'totalProducts')
                        <tr >
                            <td >{{ $value['qty'] }}</td>
                            <td >Product</td>
                            <td >{{ $value['product']['name'] }}</td>
                            <td >{{ $value['price']/$value['qty'] }}</td>
                            <td >{{ $value['price']}}</td>
                        </tr>
                        @endif
                    @endforeach
                @endif

                @if(Session::has('mail-cart') && $items =  Session::get('mail-cart')['services'])
                    @foreach($items as $key => $value)
                        @if( $key != 'totalServices')
                        <tr>
                            <td >{{ $value['qty'] }}</td>
                            <td >Service</td>
                            <td >{{ $value['service']['name'] }}</td>
                            <td >{{ $value['price'] }}</td>
                            <td >{{ $value['price']}}</td>
                        </tr>
                        @endif
                    @endforeach
                @endif
                <tr >
                    <td colspan="3"></td>
                    <th > VAT &nbsp; 16% :</th>
                    <th >Ksh: &nbsp;{{ Session::has('mail-cart') ? Session::get('mail-cart')['totalPrice'] * 0.16 : '' }}</th>
                </tr>
                <tr >
                    <td colspan="3"></th>
                    <th >Sub-total :</th>
                    <th >Ksh: &nbsp;{{ Session::has('mail-cart') ? Session::get('mail-cart')['totalPrice'] - Session::get('mail-cart')['totalPrice'] * 0.16  : '' }}</th>
                </tr>
                <tr >
                    <th colspan="3"></th>
                    <th >Total: </th>
                    <th >Ksh: &nbsp;{{ Session::has('mail-cart') ? Session::get('mail-cart')['totalPrice'] : '' }}</th>
                </tr>
            </table>
        </div>
        {{-- <div class="row" style="margin: 0px">
            <span class="col-md-6"></span>
            <span class="col-md-3 " style="padding-right: 0;">
                <span class=" btn btn-default ">Total: </span>
            </span>
            <span class="col-md-3" style="padding-right: 0;">
                <span class=" btn btn-default ">Ksh: &nbsp;{{ Session::has('mail-cart') ? Session::get('mail-cart')['totalPrice'] : '' }}
                </span>
            </span>
        </div> --}}
    </div>
    <footer>
        <div class="text-center">
            <p class="app-footer">&copy; 2017- All Rights Reserved.</p>
        </div>
    </footer>
</body>
</html>
