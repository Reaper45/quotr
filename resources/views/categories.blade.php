<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 4/1/17
 * Time: 1:15 PM
 */
?>
@extends('layouts.app')

@section('title')
    {{ config('app.name') }} | Available Categories
@endsection

@section('style')
<style>
    a:hover{
        text-decoration: none;
    }
    .card{
        padding: 10px 10px 40px 10px;
        margin: 10px;
        font-size: 1.3em;
        border-radius: 3px;
        background: transparent linear-gradient(100deg, rgb(16, 101, 140), rgb(38, 180, 236)) repeat scroll 0% 0%;
        color: #FFFFFF;
    }
    .card:hover{
        box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14),0 1px 5px 0 rgb(38, 180, 236),0 3px 1px -2px rgba(0,0,0,0.2);
    }
    .fa-tag{
        margin-top: 8px;
    }
    .app-footer{
        color:  #3b5998;
    }
</style>
@endsection

@section('content')
    <div class="container" style="margin-bottom: 60px;">
        @include('includes.messageblock')
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li class="active">Industries</li>
        </ol>

        <div class="row categories">
            @foreach( $categories->chunk(4) as $item)
            @foreach( $item as $category)
                <div class="col-md-3">
                    <a href="{{ route('get.businesses', ['id' => $category->id]) }}" >
                    <div class="card">
                        <span > {{ $category->name }}  <i class="fa fa-tag pull-right" aria-hidden="true"></i></span>
                    </div>
                    </a>
                </div>
                @endforeach
            @endforeach

        </div>
        <div class="text-center">
            {{ $categories->links() }}
        </div>

    </div>
@endsection
