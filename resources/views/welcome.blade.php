@extends('layouts.app')

@section('title')
    {{ config('app.name') }} | Welcome
@endsection

@section('style')
    <style>
        footer{
            margin-top: -70px;

        }
    </style>
@endsection

@section('content')
    @include('auth.login')

@endsection

