<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/26/17
 * Time: 2:00 PM
 */
?>
@extends('layouts.app')
@section('title')
   {{ config('app.name') }} | {{ Auth::user()->name }}
@endsection

@section('style')
<style>
    .fa{
        margin-right: 5px;
    }
    .fa-trash-o{
        margin: 0px !important;
    }
    .col-md-1{
        padding: 0;
    }
    .upload-img{
        width: 100%;
        margin-top: .875em !important;
        border: none !important;
    }
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }
    .my-btn{
        padding: 4px 12px;
        border-radius: 4px;
    }
    .alert-danger{
        border: solid 1px;
    }
    .help-block{
        color: #a94442;
    }
    .fa-facebook-official{
        color: #3b5998;
    }
    .cancel-btn{
        border: none;
    }
    .app-footer{
        color: #3b5998;
    }
</style>
@endsection

@section('content')
    <div class="account-container"></div>
    <div class="container" style="margin-bottom: 60px;">

        @include('includes.messageblock')

        <div class="row card user-details">
            <div class="col-md-2">
                @if(Storage::disk('userlogo')->has($user->logo))
                    <img class="img-thumbnail img-responsive company-logo" src="{{ route('user.image', ['filename'=> $user->logo, '_token' =>csrf_token()]) }}">
                @else
                    <img class="img-thumbnail img-responsive company-logo" src="{{ asset('/img/wireframe.png') }}">
                @endif
            </div>
            <div class="col-md-5" style="border-right: solid 1px #ccc;">
                <div class="row" style="">
                    <label for="company-name" class="col-md-4 control-label">Name :</label>

                    <div class="col-md-8" id="location">
                        <span id="company-name" class="company-name">{{ $user->name }}</span>
                    </div>
                </div>

                <div class="row" style="">
                    <label for="category" class="col-md-4 control-label">Industry :</label>

                    <div class="col-md-8" id="location">
                        @if($user->category_id != null )
                            <span id="category" class="category">{{ $user->category->name }}</span>
                        @endif
                    </div>
                </div>

                <div class="row" style="margin-top: 20px;">
                    <label for="location" class="col-md-4 control-label">Location :</label>

                    <div class="col-md-6" id="location">
                        <span class="">{{ $user->location }}</span>
                    </div>
                </div>

                <div class="row" style="">
                    <label for="email" class="col-md-4 control-label">Email :</label>

                    <div class="col-md-8" id="location">
                        <span id="email" class="email">{{ $user->email }}</span>
                    </div>
                </div>

                <div class="row" style="">
                    <label for="phone" class="col-md-4 control-label">Phone :</label>

                    <div class="col-md-8" id="location">
                        <span id="phone" class="phone">{{ $user->phone }}</span>
                    </div>
                </div>

            </div>
            <div class="col-md-5">
                <div class="row" style="margin-top: 20px; padding: inherit;">
                    <label for="location" class="control-label">Bio :</label>
                </div>
                <div class="row" id="location" style=" padding: inherit;">
                    <span class="">{{ $user->bio }}</span>
                </div>


                <div class="row social-accnt" style="margin-top: 20px;">
                    <div class="col-md-12">
                        <span style="text-align: center; margin-left: 18px;">
                            <a href="{{ $user->facebook }}" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true" style="font-size: 2.5em;"></i></a>
                            <a href="{{ $user->twitter }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true" style="font-size: 2.5em;"></i></a>
                            <a href="{{ $user->website }}" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2.5em;"></i></a>
                        </span>

                    </div>
                </div>
            </div>
        </div>
        {{--@endforeach--}}
    <!--Tabs-->
        <div class="row" style="min-height: 225px;">
            <div class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="font-weight: 700;"><i class="fa fa-home" aria-hidden="true"></i>Home </div>
                    <!-- List group / Nav tabs -->
                    <div class="list-group" role="tablist">
                        <a id="user_store" href="{{ route('auth.account') }}"  class="list-group-item">
                            <i class="fa fa-archive" aria-hidden="true"></i>Available Items</a>
                        <a id="received_quotation" href="{{ route('user.quotations') }}" class="list-group-item">
                            <i class="fa fa-tags" aria-hidden="true"></i>Sent Quotes</a>
                        <a id="sent_quotation" href="{{ route('user.requests') }}" class="list-group-item">
                            <i class="fa fa-inbox" aria-hidden="true"></i>Received Quotes</a>
                        <a id="user_editprofile" href="{{ route('user.editprofile') }}"  class="list-group-item">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit Profile</a>
                    </div>
                </div>
                <div>
                    <a href="{{ route('quotation.send') }}" style="padding: 6px 12px; margin-top: -8px; width: 100%" class="btn btn-default pull-right">
                        Send Quotation
                    </a>
                </div>
            </div>
            <div class="col-md-10">
                @yield('qtcontent')
            </div>

        </div>
    </div>
    @include('includes.addnew')
    @include('includes.editproduct')
    @include('includes.editservice')
@endsection
