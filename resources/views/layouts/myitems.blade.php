<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 4/22/17
 * Time: 4:15 PM
 */
?>
@extends('layouts.app')
@section('title')
    {{ config('app.name') }} | {{ Auth::user()->name }}
@endsection

@section('style')
    <style>
        .fa{
            margin-right: 8px;
        }
        .btn-default{
            /*padding: 1px 6px;*/
        }
        .app-footer{
            color:  rgb(16, 101, 140);
        }
    </style>

    @endsection


@section('content')
    <div class="container" style="margin-bottom: 60px;">
        @include('includes.messageblock')
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li><a href="{{ route('auth.account') }}" >{{ Auth::user()->name }} </a></li>
            <li>Send Quotation</li>
        </ol>

        <div class="row">
            <div class="col-md-2">
                <div class="panel panel-default">
                    <!-- List group / Nav tabs -->
                    <div class="list-group" role="tablist">
                        <a class="list-group-item"id="productslist" href="{{ route('quotation.send') }}">
                            <i class="fa fa-tags" aria-hidden="true"></i>Products</a>
                        <a  class="list-group-item" id="serviceslist" href="{{ route('quotation.send.services') }}">
                            <i class="fa fa-archive" aria-hidden="true"></i>Services</a>
                    </div>
                </div>
                <div><a class="btn btn-default" style="padding: 1px 6px;" href="{{ route('send.to') }}">Send Quotation</a>
                    <span class="badge" style="margin-left: 20px;"> {{ Session::has('send-cart') ? Session::get('send-cart')['totalQty'] : 0}}</span>
                </div>

            </div>
            <div class="col-md-10">
                <div class="row">
                    <!-- Tab panes -->
                    @yield('tab')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
