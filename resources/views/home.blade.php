@extends('layouts.app')
@section('title')
    {{ config('app.name') }} | Home
@endsection

@section('style')
    <style>
        body{
        }
        .auth-container{
            margin-top: -80px;
        }
        .main-top{
            height: 100vh;
            padding-top: 200px;
            /*padding-bottom: 200px;*/
        }
        input[type="search"]{
            border: none;
            margin-top: 5px;
            font-size: 1.3em;
            width: 80%;
        }

        .my-btn{
            border: solid 2px #5dc2f1 !important;
            border-radius: 25px;
            background: rgb(16, 101, 140);
            font-size: 1.5em;
            padding: 5px 40px;
        }
        .my-btn:hover{
            border-color: #0A82B9 !important;
            background: rgb(38, 180, 236);
        }

        .home-title{
            font-size: 3.5em;
        }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus{
            border-bottom: solid 3px #0097DB !important;
            border: none;
            background: none;
        }
        .nav-tabs > li.active > a:hover{
            border: none;
            background: #ccc;
        }
        footer{
            margin-top: -70px;

        }

    </style>
    <script type="application/javascript">
        {{--var getCategoryUrl = "{{ route('get.category') }}";--}}
    </script>
    @endsection

@section('content')
    @include('includes.messageblock')


    <div class="auth-container">
        <div class="main-top container">
            <div class="content">
                <div class="" style="text-align: center;">
                    <h1 class="home-title" style="color: #FFFFFF;">Get Quotations, Send Quotations</h1>
                    <div class="">
                        <div class="" style="padding: 0; margin-top: 10px;  ">
                            {{--<span class="horizontal-line"></span>--}}
                        </div>
                        <div class="">
                            <h3 class="home-title-mnr" style="color: #E0F2FB;">Quotr lets companies request, send and receive Quotations.</h3>
                        </div>
                    </div>
            </div>

            <div class="home-btns" style="text-align: center; margin-top: 60px;">
                <a href="{{ route('get.categories') }}" class="btn btn-primary my-btn">Get Quote</a>
                <span class="or" style="font-size: 1.8em; padding: 0px 25px; font-weight: bolder; color: #fff;">
                    OR
                </span>
                <a href="{{ route('quotation.send') }}" class="btn btn-primary my-btn">Send Quote</a>
            </div>
        </div>
    </div>

    </div>
    <script type="text/javascript">
    </script>
@endsection
