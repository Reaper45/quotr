<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 4/23/17
 * Time: 12:53 PM
 */
?>
@extends('layouts.myitems')

@section('tab')
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="sendProducts">

        @if($products != null)
            @foreach($products->chunk(4) as $item)
                <div class="row">
                    @foreach($item as $key => $product )
                        <div class="col-md-3" style="">
                            <div class="thumbnail">
                                @if(Storage::disk('productImages')->has($product->image))
                                    <img id="click" src="{{ route('product.image', ['filename'=>$product->image]) }}">
                                @else
                                    <img class="img-responsive" src="{{ URL::to('img/wireframe.png') }}" style="background: #fff;">
                                @endif
                                <div class="caption">
                                    <p><label>{{ $product->name }}</label> <span class="pull-right">{{ $product->qty ? "Qty: ".$product->qty : ""}}</span></p>
                                    <div>
                                        <label>Ksh. {{ $product->price }}</label>
                                        <form action="{{ route('add.quotation.send', ['id'=>$product->id]) }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="number" min="1" class="form-control" value="1" name="qty" required>
                                                </div>
                                                <div class="col-md-6">

                                                    @if( Session::get('send-cart')['products'] !=null && array_key_exists($product->id, Session::get('send-cart')['products']))
                                                        <button type="submit" class="btn btn-default pull-right">Update</button>
                                                    @else
                                                        <button type="submit" class="btn btn-default pull-right">Add</button>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                        {{--<a class="btn btn-default pull-right" role="button" href="{{ route('add.quotation.send', ['id'=>$product->id]) }}">Add</a>--}}
                                        <ul class="dropdown-menu" role="menu" style="min-width: 130px;">
                                            <input id="id" type="hidden" value="{{ $product->id }}">
                                            <input id="name" type="hidden" value="{{ $product->name }}">
                                            <input id="price" type="hidden" value="{{ $product->price }}">
                                            <input id="desc" type="hidden" value="{{ $product->description }}">
                                            <input id="qty" type="hidden" value="{{ $product->qty }}">
                                            {{--<li><a id="edit-product" class="btn edit-product" data-toggle="modal" data-target="#editProduct" style="text-align: left;">Edit</a></li>--}}
                                            {{--<li><a href="{{ route('product.delete', ['id'=> $product->id]) }}">Delete</a></li>--}}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        @endif
            <div class="text-center">
                {{ $products->links() }}
            </div>
    </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#productslist').addClass('active')
    </script>
@endsection
