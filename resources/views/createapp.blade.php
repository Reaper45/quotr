<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/25/17
 * Time: 1:10 PM
 */
?>

@extends('layouts.app')

@section('title')
    {{ config('app.name') }} | CreateNew App
    @endsection

@section('style')
    <style>

        .app-footer{
            color:  #3b5998;
        }
    </style>
@endsection


@section('content')
    @include('includes.messageblock')
    <div class="container" style="margin-bottom: 60px;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if(Session::has('myapp'))
                        <div class="panel-heading">App Credentials</div>
                        <div class="panel-body">
                            <div class="form-group">
                                @foreach(Session::get('myapp') as $key => $value)
                                    <label for="app_name" class="col-md-4 control-label">{{ $key }}</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" value="{{ $value }}">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @else
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('createapp') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">App Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create New App
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                        @endif
                </div>
            </div>
        </div>
    </div>
    @endsection