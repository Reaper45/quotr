<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 4/7/17
 * Time: 5:52 PM
 */

?>
@extends('layouts.app')

@section('title')
    {{ config('app.name')  }} | {{ $service->name }}
@endsection

@section('style')
    <style>
        img{

        }
        .app-footer{
            color:  #3b5998;
        }
    </style>
@endsection

@section('content')
<div class="container" style="margin-bottom: 60px;">
    <ol class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li><a href="{{route('get.users', ['userid'=>$service->user->id])}}">{{ $service->user->name }}</a></li>
        <li>Services</li>
    </ol>

    @include('includes.messageblock')

    <h2>{{ $service->name }} </h2>
    <hr/>
    <div class="row">
        <div class="col-md-4">
            @if(Storage::disk('serviceImages')->has($service->image))
                <img style="width: 100%;" id="click" src="{{ route('service.image', ['filename'=>$service->image]) }}">
            @else
                <img style="width: 100%;" class="img-responsive" src="{{ URL::to('img/wireframe.png') }}" style="background: #fff;">
            @endif
        </div>
        <div class="col-md-8">
            <h3>Price: {{ $service->price }}</h3>
            <p>{{ $service->description }}</p>
            <a href="{{ route('add.service.to.cart',['id'=>$service->id]) }}" class="btn btn-primary my-btn">Add to Quote Cart</a>
        </div>
    </div>

    <div>
        @if($services != null)
            <h3 style="margin-top: 50px;">More Services From {{ $service->user->name }}</h3><hr/>
            @include('includes.service')
        @endif
    </div>
</div>
@endsection
