<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 4/7/17
 * Time: 5:52 PM
 */
?>
@extends('layouts.app')

@section('title')
    {{ config('app.name')  }} | {{ $product->name }}
@endsection

@section('style')
    <style>

        .app-footer{
            color:  #3b5998;
        }
    </style>
@endsection

@section('content')
    <div class="container"  style="margin-bottom: 60px;">
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li><a href="{{route('get.users', ['userid'=>$product->user->id])}}">{{ $product->user->name }}</a></li>
            <li>Products</li>
        </ol>
        @include('includes.messageblock')
        <h2>{{ $product->name }} </h2>
        <hr/>
        <div class="row">
            <div class="col-md-4">
                @if(Storage::disk('productImages')->has($product->image))
                    <img style="width: 100%;" id="click" src="{{ route('product.image', ['filename'=>$product->image]) }}">
                @else
                    <img style="width: 100%;" class="img-responsive" src="{{ URL::to('img/wireframe.png') }}" style="background: #fff;">
                @endif
            </div>
            <div class="col-md-8">
                <h3>Price: {{ $product->price }}</h3>
                <h4>{{ $product->qty ? "Remaining: ".$product->qty." Units": ""}}</h4>
                <p>{{ $product->description }}</p>
                <form action="{{ route('add.product.to.cart', ['id'=>$product->id]) }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="item" value="{{ $product }}">
                    <div class="form-group" id="qtyAvailable">
                        <label for="qty" class="control-label">Quantity: </label>
                        <div class="input-group">
                            <input type="number" min="1" class="form-control" value="1" name="qty" required>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary my-btn">Add to Quote Cart</button>
                </form>
            </div>
        </div>

        <div>
            @if($products != null)
                <h3 style="margin-top: 50px;">More Products From {{ $product->user->name }}</h3><hr/>
                @include('includes.product')
            @endif
        </div>
    </div>
@endsection
