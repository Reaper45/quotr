
@extends('layouts.app')
@section('title')
    {{ config('app.name') }} | Send-To
@endsection

@section('style')
    <style>
    body{
        min-width: 350px;
    }

    </style>
@endsection

@section('content')
    <div class="" >
        <li class="alert alert-success alert-dismissible message" role="alert" style="list-style: none;">
            <strong><i style="font-size: 1.5em; padding-right: 5px;"  class="fa fa-check-circle" aria-hidden="true"></i>Done!</strong>
            {{ Session::get('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button></li>
    </div>
@endsection
