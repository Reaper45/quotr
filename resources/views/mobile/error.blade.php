<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 6/18/17
 * Time: 02:17 PM
 */
?>

@extends('layouts.app')
@section('title')
    {{ config('app.name') }} | Send-To
@endsection

@section('style')
    <style>
    body{
        min-width: 350px;
    }
    </style>
@endsection

@section('content')
    <div class="">
        <li class="alert alert-danger alert-dismissible message" role="alert" style="list-style: none;">
            <strong><i style="font-size: 1.5em; padding-right: 5px;" class="fa fa-minus-circle" aria-hidden="true"></i>Failed!</strong>
            {{ Session::get('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button></li>
    </div>
@endsection
