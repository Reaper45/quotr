<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 6/18/17
 * Time: 02:17 PM
 */
?>

@extends('layouts.app')
@section('title')
    {{ config('app.name') }} | Send-To
@endsection

@section('style')
    <style>
    body{
        min-width: 350px;
    }
    .btn{
        padding: 0px 6px;
        border-color: transparent !important;
    }
    .img-circle{
        border: solid 1px;
    }
    </style>
@endsection

@section('content')
<div>
    <ol class="breadcrumb my-breadcrumb">
        <li>Client: </li>
        <li class="active">
            <a href="#">{{ $cart['to']->name }}</a>
        </li>

        <div class="pull-right">
            @if(Storage::disk('userlogo')->has($cart['to']->logo))
                <img class="img-circle" style="margin-top: -30px; width:50px; height:50px;" src="{{ route('user.image', ['filename'=>  $cart['to']->logo, '_token' =>csrf_token()]) }}">
            @else
                <img class="img-circle" style="margin-top: -30px; width:50px; height:50px;" src="{{ asset('/img/wireframe.png') }}">
            @endif
            {{-- <img class="img-circle" style="margin-top: -30px; width:50px; height:50px;" src="{{ asset('/img/wireframe.png') }}" alt="Joram"> --}}
        </div>
    </ol>

    <table class="table table-bordered">
        <tr>
            <th class="">Item</th>
            <th class="">Units</th>
            <th class="">Per Unit</th>
            <th class="">Total</th>
            {{-- <th class=""></th> --}}
        </tr>

        @if ($cart['products'] !== null)
            @foreach($cart['products'] as $key => $value)
                <tr>
                @if( $key != 'totalProducts')
                    <td class="">{{ $value['product']['name'] }}</td>
                    <td class="">{{ $value['qty'] }}</td>
                    <td class="">{{ $value['price']/$value['qty'] }}</td>
                    <td class="">{{ $value['price'] }}</td>
                    {{-- <td style="width: 40px !important;" class="">
                        <a href="#" class="btn btn-primary cancel-btn" title="Remove">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    </td> --}}
                @endif
            </tr>
            @endforeach
        @endif

        @if ($cart['services'] !== null)
            @foreach($cart['services'] as $key => $value)
                <tr>
                @if( $key != 'totalServices')
                    <td class="">{{ $value['service']['name'] }}</td>
                    <td class="">{{ $value['qty'] }}</td>
                    <td class="">{{ $value['price']/$value['qty'] }}</td>
                    <td class="">{{ $value['price'] }}</td>
                    {{-- <td style="width: 40px !important;" class="">
                        <a href="#" class="btn btn-primary cancel-btn" title="Remove">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    </td> --}}
                @endif
                </tr>
            @endforeach
        @endif

    </table>

    <div class="row" style="margin: 10px 0px">
        <div class="col-md-6 pull-right" style="padding-right: 0; margin-right: 10px;">
            Sub-total :
            <span class="col-md-6" style="padding-right: 0;">
                Ksh: &nbsp;
            </span>

            <span class=" btn btn-default " >{{ $cart['totalPrice'] ?: "0.00"  }}</span>
        </div>
    </div>

    <div class="row" style="margin: 30px 10px auto 0px;">
        {{-- @if(Session::has('send-cart')) --}}
            <div class="pull-right" style="margin-top: -8px;">
                <a class="btn btn-default my-btn cancel-btn" href="{{ route('empty.quotation.send') }}">Empty</a>
                <a href="{{ route('mobile.quotation.finish') }}" class="btn btn-default my-btn my-btn-blue" >Send</a>
            </div>
        {{-- @endif --}}
    </div>
</div>
@endsection
