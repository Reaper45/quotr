<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 4/3/17
 * Time: 4:18 PM
 */
?>

@extends('layouts.app')

@section('title')
    {{ config('app.name') }} | {{ $category->name }}
@endsection

@section('style')
    <style>
        .thumbnail{
            padding: 0px;
        }
        .thumbnail .caption{
            padding: 15px 9px;
            background: #f5f5f5;
        }
        .app-footer{
            color:  #3b5998;
        }
    </style>
@endsection

@section('content')
    <div class="container"  style="margin-bottom: 60px;">
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li><a href="/categories">Industries</a></li>
            <li class="active">{{ $category->name }}</li>
        </ol>

        <div class="">
            @if (count($users) > 0)
                @foreach($users->chunk(4) as $item)
                    <div class="row">
                    @foreach($item as $user)
                        @if( $user->id != Auth::user()->id || Auth::guest())
                        <div class="col-md-3" style="margin-top: 20px;">
                            <div class="thumbnail">
                            @if(Storage::disk('userlogo')->has($user->logo))
                                <img style="max-height: 100%; width: 100%;" id="click" src="{{ route('user.image', ['filename'=>$user->logo]) }}">
                            @else
                                <img class="img-responsive" src="{{ URL::to('img/wireframe.png') }}" style="background: #fff;">
                            @endif
                            <div class="caption">
                                <strong style="font-size: 1.2em;">{{ $user->name }}</strong>
                                <a style="padding: 3px 12px;" class="btn btn-primary my-btn-blue pull-right" href="{{ route('get.users', ['userid'=>$user->id]) }}">Visit</a>
                            </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                    </div>
                @endforeach
            @else
                <div class="alert alert-info">
                    <p class="text-center fa-2x">There are no Businesses under {{ $category->name }} yet!</p>
                </div>
            @endif
        </div>
    </div>
@endsection
