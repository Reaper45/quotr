
{{--@foreach($user as $user)--}}
    <div class="row card user-details">
        <div class="col-md-2">
            @if(Storage::disk('userlogo')->has($user->logo))
                <img class="img-thumbnail img-responsive company-logo" src="{{ route('user.image', ['filename'=> $user->logo, '_token' =>csrf_token()]) }}">
            @else
                <img class="img-thumbnail img-responsive company-logo" src="{{ asset('/img/wireframe.png') }}">
            @endif
        </div>
        <div class="col-md-5" style="border-right: solid 1px #ccc;">
            <div class="row" style="">
                <label for="company-name" class="col-md-4 control-label">Name :</label>

                <div class="col-md-8" id="location">
                    <span id="company-name" class="company-name">{{ $user->name }}</span>
                </div>
            </div>

            <div class="row" style="">
                <label for="category" class="col-md-4 control-label">Category :</label>

                <div class="col-md-8" id="location">
                    @if($user->category_id != null )
                    <span id="category" class="category">{{ $user->category->name }}</span>
                        @endif
                </div>
            </div>

            <div class="row" style="margin-top: 20px;">
                <label for="location" class="col-md-4 control-label">Location :</label>

                <div class="col-md-6" id="location">
                    <span class="">{{ $user->location }}</span>
                </div>
            </div>

            <div class="row" style="">
                <label for="email" class="col-md-4 control-label">Email :</label>

                <div class="col-md-8" id="location">
                    <span id="email" class="email">{{ $user->email }}</span>
                </div>
            </div>

            <div class="row" style="">
                <label for="phone" class="col-md-4 control-label">Phone :</label>

                <div class="col-md-8" id="location">
                    <span id="phone" class="phone">{{ $user->phone }}</span>
                </div>
            </div>

        </div>
        <div class="col-md-5">
            <div class="row" style="margin-top: 20px; padding: inherit;">
                <label for="location" class="control-label">Bio :</label>
            </div>
            <div class="row" id="location" style=" padding: inherit;">
                <span class="">{{ $user->bio }}</span>
            </div>


            <div class="row social-accnt" style="margin-top: 20px;">
                <div class="col-md-12">
                        <span style="text-align: center; margin-left: 18px;">
                            <a href="{{ $user->facebook }}" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true" style="font-size: 2.5em;"></i></a>
                            <a href="{{ $user->twitter }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true" style="font-size: 2.5em;"></i></a>
                            <a href="{{ $user->website }}" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2.5em;"></i></a>
                        </span>
                    <!-- @if($user->id == Auth::user()->id)
                    <a href="#editprofile" aria-controls="editprofile" role="tab" data-toggle="tab" class="edit-cart-item btn btn-primary my-btn-blue my-btn pull-right" id="addnew-btn">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        Edit Profile
                    </a>
                        @endif -->
                </div>
            </div>
        </div>
    </div>
    {{--@endforeach--}}
    <!--Tabs-->
    <div class="row" style="min-height: 225px;">
        @if($user->id == Auth::user()->id)
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="font-weight: bolder;"><i class="fa fa-tachometer" aria-hidden="true"></i>Profile</div>
                <!-- List group / Nav tabs -->
                <div class="list-group" role="tablist">
                    <a href="#store" aria-controls="store" role="tab" data-toggle="tab" class="list-group-item active" role="presentation">
                        <i class="fa fa-archive" aria-hidden="true"></i>Available Items</a>
                    <a href="#myqoutations" aria-controls="myqoutations" role="tab" data-toggle="tab" class="list-group-item" role="presentation">
                        <i class="fa fa-tags" aria-hidden="true"></i>Sent Quotes</a>
                    <a href="#myrequests" aria-controls="myrequests" role="tab" data-toggle="tab" class="list-group-item" role="presentation">
                        <i class="fa fa-inbox" aria-hidden="true"></i>Recieved Quotes</a>
                    <a href="#editprofile" aria-controls="editprofile" role="tab" data-toggle="tab" class="list-group-item" role="presentation" id="edit-tab-item">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit Profile</a>
                </div>
            </div>
            <div>
                <a href="{{ route('quotation.send') }}" style="padding: 6px 12px; margin-top: -8px; width: 100%" class="btn btn-default pull-right">
                    Send Quotation
                </a>
            </div>
        </div>
        @endif

        @if($user->id != Auth::user()->id)
            <div class="col-md-12">
        @else
            <div class="col-md-10">
            @endif
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="store">
                    @include('templates.store')
                </div>
                @if($user->id == Auth::user()->id)
                    <div role="tabpanel" class="tab-pane" id="myqoutations">
                        @include('templates.recieved-quotations')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="myrequests">
                        @include('templates.sent-quotations')
                    </div>

                    <div role="tabpanel" class="tab-pane" id="editprofile">
                        @include('templates.editprofile')
                    </div>
                @endif
            </div>
        </div>
    </div>