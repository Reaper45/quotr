<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/26/17
 * Time: 1:46 PM
 */
?>
@extends('layouts.app')
@section('title')
    {{ config('app.name') }} |@if(isset($user)) {{ $user->name }} @endif
    @endsection

@section('style')
    <style>
        .my-btn{
            border-radius: 4px;
        }
        .app-footer{
            color:  #3b5998;
        }
    </style>
    @endsection

@section('content')

    <div class="container" style="margin-bottom: 60px;">
        @include('includes.messageblock')
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li><a href="/categories">Industries</a></li>
            <li><a href="{{ route('get.businesses', ['id' => $user->category_id]) }}">{{ $user->category->name }}</a></li>
            {{--<li><a href="#">User</a></li>--}}
            <li class="active">@if(isset($user)) {{ $user->name }} @endif</li>
        </ol>

        @if(isset($user))
            @include('templates.profiletemplate')
            @endif

    </div>
    @endsection
