<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/26/17
 * Time: 7:00 PM
 */
?>
@if($products != null)
@foreach($products->chunk(4) as $item)
<div class="row">
    @foreach($item as $key => $product )
    <div class="col-md-3" style="margin-top: 20px;">
        <div class="thumbnail">
            @if(Storage::disk('productImages')->has($product->image))
                <img id="click" src="{{ route('product.image', ['filename'=>$product->image]) }}">
            @else
                <img class="img-responsive" src="{{ URL::to('img/wireframe.png') }}" style="background: #fff;">
            @endif
                <div class="caption">
                <p><label>{{ $product->name }}</label> <span class="pull-right">{{  $product->qty ? "Qty: ".$product->qty : "" }}</span></p>
                <div>
                <label>Ksh. {{ $product->price }}</label>
                    @if($product->user_id != Auth::user()->id)
                    <a style="padding: 6px 12px; margin-top: -8px;" class="btn btn-primary my-btn pull-right"
                       href="{{ route('product.details', ['user_id'=>$product->user->id, 'id'=> $product->id, ]) }}">
                        View Details
                    </a>
                        @else
                            <span  class="dropdown">
                                <a class="dropdown-toggle pull-right" data-toggle="dropdown" role="button" aria-expanded="false" href="#"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu" role="menu" style="min-width: 130px;">
                                    <input id="id" type="hidden" value="{{ $product->id }}">
                                    <input id="name" type="hidden" value="{{ $product->name }}">
                                    <input id="price" type="hidden" value="{{ $product->price }}">
                                    <input id="desc" type="hidden" value="{{ $product->description }}">
                                    <input id="qty" type="hidden" value="{{ $product->qty }}">
                                    <li><a id="edit-product" class="btn edit-product" data-toggle="modal" data-target="#editProduct" style="text-align: left;">Edit</a></li>
                                    <li><a href="{{ route('product.delete', ['id'=> $product->id]) }}">Delete</a></li>
                                </ul>
                            </span>
                        @endif
                </div>
                </div>
        </div>
    </div>
    @endforeach
</div>

@endforeach
@endif
<div class="text-center">
    {{ $products->links() }}
</div>
