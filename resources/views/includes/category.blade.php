<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/27/17
 * Time: 12:59 PM
 */
?>

<div class="form-group">
    <label for="category">Industry</label>
    <select class="form-control" id="category" name="category">

            @foreach($categories as $category)
            @if(isset($user->category_id))
                @if($user->category_id == $category->id)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endif
            @else
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endif
            @endforeach

        {{--@foreach($categories as $category)--}}
            {{----}}
                {{--<option value="{{ $category->id }}">{{ $category->name }}</option>--}}
            {{----}}
        {{--@endforeach--}}

    </select>
</div>
