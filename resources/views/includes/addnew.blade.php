
<div class="modal fade" id="newPost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">New Product / Service</h4>
            </div>
            <div class="modal-body">
                <form id="newpostForm" action="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name" class="control-label">Title:</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="category" class="control-label">Product or Service:</label>
                        <select class="form-control" id="itemcategory" name="category">>
                            <option value="product">Product</option>
                            <option value="service">Service</option>

                        </select>
                    </div>
                    <div class="form-group col-md-6" id="priceCol">
                    <label for="price" class="control-label">Price</label>
                    <div class="input-group">
                        <span class="input-group-btn input-group-addon">
                                <i class="" aria-hidden="true" style="font-size: initial;">Ksh: </i>
                            </span>
                        <input id="price" type="number" min="0" class="form-control" name="price" placeholder="Price" required>

                    </div>
                    </div>
                    <div class="form-group col-md-6" id="qtyAvailable">
                        <label for="qty" class="control-label">How Many Available</label>
                        <div class="input-group">
                            <input id="qty" type="number" min="1" class="form-control" name="qty" placeholder="Optional">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image" class="control-label">Upload Image</label><span class="pull-right text-info">(max size: )</span>
                        <input type="file" id="image" name="image" style="width: 100%; padding: 3px;" required>
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label">Short Description: </label><span class="pull-right text-info">(max: 120 words)</span>
                        <textarea class="form-control" id="description" name="description" maxlength="120" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                {{--<button type="button" class="btn btn-default get-started-btn" data-dismiss="modal" style="">Close</button>--}}
                <button form="newpostForm" type="submit" class="btn btn-primary get-started-btn"><i class="fa fa-floppy-o" aria-hidden="true" disabled="true "></i>Save</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var postServiceUrl = "{{ route('new.service') }}";
    var postProductUrl = "{{ route('new.product') }}";
</script>
