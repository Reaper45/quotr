<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/22/17
 * Time: 7:11 PM
 */
?>
@if($errors->count()>0 )
    <div class="row" >
        <div class="col-md-12">
            @foreach($errors->all() as $error)
                <li class="alert alert-danger alert-dismissible message" role="alert" style="list-style: none;">
                    <strong><i style="font-size: 1.5em; padding-right: 5px;" class="fa fa-minus-circle" aria-hidden="true"></i>Failed!</strong>
                    {{ $error }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button></li>
            @endforeach
        </div>
    </div>
@endif

@if(Session::has('success'))
    <div class="row" >
        <div class="col-md-12">
            <div class="" >
                <li class="alert alert-success alert-dismissible message" role="alert" style="list-style: none;">
                    <strong><i style="font-size: 1.5em; padding-right: 5px;"  class="fa fa-check-circle" aria-hidden="true"></i>Done!</strong>
                    {{ Session::get('success')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button></li>
            </div>
        </div>
    </div>
@endif

@if(Session::has('info'))
    <div class="row" >
        <div class="col-md-12">
            <div class="" >
                <li class="alert alert-info alert-dismissible message" role="alert" style="list-style: none;">
                    <strong><i style="font-size: 1.5em; padding-right: 5px;"  class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>Heads Up!</strong>
                    {{ Session::get('info')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button></li>
            </div>
        </div>
    </div>
@endif



{{--@if(Session::has('message'))--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="alert-success" >--}}
            {{--<li class="alert alert-success message text-center" style="list-style: none;">{{ Session::get('message')}}</div></li>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@endif--}}

