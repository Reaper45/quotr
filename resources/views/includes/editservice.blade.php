<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 4/16/17
 * Time: 1:07 PM
 */
?>

<div class="modal fade" id="editService" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Edit Item</h4>
            </div>
            <div class="modal-body">
                <form id="editServiceForm" action="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label for="name" class="control-label">Title:</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                    <div class="form-group" id="priceCol">
                        <label for="price" class="control-label">Price</label>
                        <div class="input-group">
                            <span class="input-group-btn input-group-addon">
                                <i class="" aria-hidden="true" style="font-size: initial;">Ksh: </i>
                            </span>
                            <input id="price" type="number" min="0" class="form-control" name="price" placeholder="Price" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="control-label">Upload Image</label><span class="pull-right text-info">(Leave Blank If don't Want to Change Image )</span>
                        <input type="file" id="image" name="image" style="width: 100%; padding: 3px;">
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label">Short Description: </label><span class="pull-right text-info">(max: 120 words)</span>
                        <textarea class="form-control" id="description" name="description" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                {{--<button type="button" class="btn btn-default get-started-btn" data-dismiss="modal" style="">Close</button>--}}
                <button form="editServiceForm" type="submit" class="btn btn-primary get-started-btn"><i class="fa fa-floppy-o" aria-hidden="true" disabled="true "></i>Update Product</button>
            </div>
        </div>
    </div>
</div>
