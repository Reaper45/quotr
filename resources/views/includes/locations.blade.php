<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/30/17
 * Time: 8:19 PM
 */
?>

<div class="form-group">
    <label for="location">Location</label>
    <select class="form-control" id="location" name="location">
        @if(isset($user->location))
            <option value="{{ $user->location }}">{{ $user->location }}</option>
            @else
        <option value="">Select Location</option>
        <option value="Nairobi, KE">Nairobi, KE</option>
        <option value="Mombasa, KE">Mombasa, KE</option>
        <option value="Nakuru, KE">Nakuru, KE</option>
        <option value="Kisumu, KE">Kisumu, KE</option>
        <option value="Kitale, KE">Kitale, KE</option>
        <option value="Malindi, KE">Malindi, KE</option>
        <option value="Nanyuki, KE">Nanyuki, KE</option>
        <option value="Lodwar, KE">Lodwar, KE</option>
        <option value="Voi, KE">Voi, KE</option>
        <option value="naivasha, KE">naivasha, KE</option>
        <option value="Siaya, KE">Siaya, KE</option>
        <option value="Kerugoya, KE">Kerugoya, KE</option>
        <option value="Kitui, KE">Kitui, KE</option>
        <option value="Meru, KE">Meru, KE</option>
        <option value="Meru, KE">Meru, KE</option>
        <option value="Embu, KE">Embu, KE</option>
        <option value="Kakamega, KE">Kakamega, KE</option>
        <option value="Chuka, KE">Chuka, KE</option>
        <option value="Limuru, KE">Limuru, KE</option>
        <option value="Lamu, KE">Lamu, KE</option>
        <option value="Narok, KE">Narok, KE</option>
        <option value="Homabay, KE">Homabay, KE</option>
        <option value="Marsabit, KE">Marsabit, KE</option>
        <option value="Malaba, KE">Malaba, KE</option>
        <option value="Mandera, KE">Mandera, KE</option>
            @endif
    </select>
</div>
