<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/26/17
 * Time: 7:00 PM
 */
?>
@if($services != null )
@foreach($services->chunk(4) as $item)
<div class="row">
    @foreach($item as $key => $service )
        <div class="col-md-3" style="margin-top: 20px;">
            <div class="thumbnail">
                @if(Storage::disk('serviceImages')->has($service->image))
                    <img id="click" src="{{ route('service.image', ['filename'=>$service->image]) }}">
                @else
                    <img class="img-responsive" src="{{ URL::to('img/wireframe.png') }}" style="background: #fff;">
                @endif
                <div class="caption">
                    <label>{{ $service->name }}</label>
                <div>
                <label>Ksh. {{ $service->price }}</label>
                @if($service->user_id != Auth::user()->id)
                    <a style="padding: 6px 12px; margin-top: -8px;" class="btn btn-primary my-btn pull-right"
                       href="{{ route('service.details', ['user_id'=>$service->user->id, 'id'=> $service->id, ]) }}"
                       class="btn btn-default pull-right">
                        View Details
                    </a>
                    @else
                        <span  class="dropdown">
                            <a class="dropdown-toggle pull-right" data-toggle="dropdown" role="button" aria-expanded="false" href="#"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu" style="min-width: 130px;">
                                <input id="id" type="hidden" value="{{ $service->id }}">
                                <input id="name" type="hidden" value="{{ $service->name }}">
                                <input id="price" type="hidden" value="{{ $service->price }}">
                                <input id="desc" type="hidden" value="{{ $service->description }}">
                                <li><a id="edit-service" class="btn edit-service" data-toggle="modal" data-target="#editService" style="text-align: left;">Edit</a></li>
                                <li><a href="{{ route('service.delete', ['id'=> $service->id]) }}">Delete</a></li>
                            </ul>
                        </span>
                @endif
                </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endforeach
    @endif
<div class="text-center">
    {{ $services->links() }}
</div>

