<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 4/1/17
 * Time: 11:57 AM
 */
?>

@extends('layouts.app')

@section('title')
    {{ config('app.name') }} | My Quote Cart
    @endsection

@section('style')
    <style>
        body{
            min-width: 600px;
        }
        .navbar{
            margin-bottom: 0;
        }
        .btn-primary{
            padding: 0px 6px;
            border: none;
        }
        .app-footer{
            color:  #3b5998;
        }
        .account-container{
            position: initial;
            height: 50px;
        }
        .my-breadcrumb > li + li::before{
            content: '';
        }
        .my-breadcrumb{
            border-radius: 25px;
        }
    </style>

@endsection


@section('content')
    @php($sendTo = Session::has('cart') ? \App\User::find( Session::get('cart')['quoteTo']) : null )
    @endphp
    <div class="account-container"></div>
    <div class="container" style="padding: 20px 10px; ">
        @include('includes.messageblock')
        <div>
            <span class="fa-3x">Quotation</span>
            <div class="thumbnail pull-right" style="width: 200px">
                @if(Storage::disk('userlogo')->has($sendTo->logo))
                    <img class="img-thumbnail img-responsive company-logo" src="{{ route('user.image', ['filename'=> $sendTo->logo, '_token' =>csrf_token()]) }}">
                @else
                    <img class="img-thumbnail img-responsive company-logo" src="{{ asset('/img/wireframe.png') }}">
                @endif
                <labe>Business: &nbsp;</labe><span  style="font-size: 1.3em;"><u>{{ $sendTo->name}}</u></span>
            </div>
        </div>
        <div>
            <span class="fa-2x">No. 0001</span>
            <span style="margin-left: 20px;">{{ date('F j, Y, g:i a', time()) }}</span>
            {{--<div class="thumbnail">--}}
            {{--@if(Storage::disk('userlogo')->has(Auth::user()->logo))--}}
            {{--<img class="img-thumbnail img-responsive company-logo" src="{{ route('user.image', ['filename'=> Auth::user()->logo, '_token' =>csrf_token()]) }}">--}}
            {{--@else--}}
            {{--<img class="img-thumbnail img-responsive company-logo" src="{{ asset('/img/wireframe.png') }}">--}}
            {{--@endif--}}
            {{--</div>--}}
        </div>

        <ol class="breadcrumb my-breadcrumb">
            <li>Client: </li>
            <li class="active"><a href="#">{{ Auth::check() ? Auth::user()->name : '' }}</a></li>
        </ol>
        <ol class="breadcrumb my-breadcrumb">
            <li>City: </li>
            <li class="active"><a href="#">{{   Auth::check() ? Auth::user()->location: '' }}</a></li>
        </ol>

        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item row" style="margin-right: 0px; margin-left: 0px; font-weight: 800;">
                    <div class="col-md-2">Qty</div>
                    <div class="col-md-2">Products/Service</div>
                    <div class="col-md-2">Item Name</div>
                    <div class="col-md-2">Unit Price</div>
                    <div class="col-md-2">Total</div>
                    <div class="col-md-2">Action</div>

                </li>
                @if(Session::has('cart') && $items =  Session::get('cart')['products'])
                    @foreach($items as $key => $value)
                        @if( $key != 'totalProducts')
                            <li class="list-group-item row" style="margin-right: 0px; margin-left: 0px;">
                                <div class="col-md-2">{{ $value['qty'] }}</div>
                                <div class="col-md-2">Product</div>
                                <div class="col-md-2">{{ $value['product']['name'] }}</div>
                                <div class="col-md-2">{{ $value['price']/$value['qty'] }}</div>
                                <div class="col-md-2">{{ $value['price']}}</div>
                                <div class="col-md-2">
                                    <a href="{{ route('product.details', ['user_id'=>Session::get('cart')['quoteTo'], 'id'=> $key ]) }}" class="btn btn-primary" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{ route('cart.remove.product',['id'=>$key]) }}" class="btn btn-primary cancel-btn" title="Remove">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </li>
                        @endif
                    @endforeach
                @endif

                @if(Session::has('cart') && $items =  Session::get('cart')['services'])
                    @foreach($items as $key => $value)
                        @if( $key != 'totalServices')
                            <li class="list-group-item row" style="margin-right: 0px; margin-left: 0px;">
                                <div class="col-md-2">{{ $value['qty'] }}</div>
                                <div class="col-md-2">Service</div>
                                <div class="col-md-2">{{ $value['service']['name'] }}</div>
                                <div class="col-md-2">{{ $value['price'] }}</div>
                                <div class="col-md-2">{{ $value['price']}}</div>
                                <div class="col-lg-2">
                                    <a href="{{ route('cart.remove.service',['id'=>$key]) }}" class="btn btn-primary cancel-btn" title="Remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </div>
                            </li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="row" style="margin: 0px">
            <span class="col-md-2 col-md-offset-8" style="padding-right: 0;">
                <span class=" btn btn-default" style="width: 100%;">VAT &nbsp; 16% :</span>
            </span>
            <span class="col-md-2" style="padding-right: 0;">
                <span class=" btn btn-default "  style="width: 100%;">Ksh: &nbsp;{{ Session::has('cart') ? Session::get('cart')['totalPrice'] * 0.16 : '' }}
                </span>
            </span>
        </div>
        <div class="row" style="margin: 10px 0px">
            <span class="col-md-2 col-md-offset-8" style="padding-right: 0;">
                <span class=" btn btn-default" style="width: 100%;">Sub-total :</span>
            </span>
            <span class="col-md-2" style="padding-right: 0;">
                <span class=" btn btn-default "  style="width: 100%;">Ksh: &nbsp;{{ Session::has('cart') ? Session::get('cart')['totalPrice'] - Session::get('cart')['totalPrice'] * 0.16  : '' }}
                </span>
            </span>
        </div>
        <div class="row" style="margin: 0px">
            {{-- <span class="col-md-8"></span> --}}
            <span class="col-md-2 col-md-offset-8" style="padding-right: 0;">
                <span class=" btn btn-default" style="width: 100%;">Total: </span>
            </span>
            <span class="col-md-2" style="padding-right: 0;">
                <span class=" btn btn-default "  style="width: 100%;">Ksh: &nbsp;{{ Session::has('cart') ? Session::get('cart')['totalPrice'] : '' }}
                </span>
            </span>
        </div>
        <div class="row" style="margin: 30px 0px auto 0px;">
            @if(Session::has('cart'))
                <div class="pull-right" style="margin-top: -8px;">
                    <a class="btn btn-default my-btn cancel-btn" href="{{ route('cart.empty') }}">Empty Quote Cart</a>
                    <a href="{{ route('get.quotation.finish') }}" class="btn btn-default my-btn my-btn-blue" >Get Quote</a>
                </div>
            @endif
        </div>
    </div>

@endsection
