<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/27/17
 * Time: 3:54 AM
 */
?>
@extends('layouts.account')
@section('title')
    {{ Auth::user()->name }} | Sent Quotations
@endsection

@section('qtcontent')

<div class="content">
    @if(count($userrequests) > 0 )
    <div class="panel panel-default">
        <div class="panel-heading" style="background: #f5f5f5; color: #636b6f; font-weight: bolder; font-size: 1.2em;">
            Account \ Requests
        </div>
        <ul class="list-group">
            <li class="list-group-item row" style="margin-right: 0px; margin-left: 0px; font-weight: 800;">
                <div class="col-lg-1">ID</div>
                <div class="col-lg-2">Sent To</div>
                <div class="col-lg-2">Date</div>
                <div class="col-lg-2">Products Total</div>
                <div class="col-lg-2">Services Total</div>
                <div class="col-lg-2">Total Quot</div>
                <div class="col-lg-1">Action</div>
            </li>
            @foreach($userrequests as $userrequest)
                @if( $userrequest->quotrdelete == false )
                    <li class="list-group-item row" style="margin-right: 0px; margin-left: 0px;">
                        <div class="col-md-1">{{ $userrequest->id }}</div>
                        <div class="col-md-2">{{ $userrequest->madeto }}</div>
                        <div class="col-md-2">{{ $userrequest->created_at }}</div>
                        <div class="col-md-2 text-center">{{ $userrequest->totalproducts ? $userrequest->totalproducts : '-'}}</div>
                        <div class="col-md-2 text-center">{{ $userrequest->totalservices ? $userrequest->totalservices : '-' }}</div>
                        <div class="col-md-2">{{ $userrequest->totalprice }}</div>
                        <div class="col-md-1">
                            <form id="delete-request" action="" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $userrequest->id }}">
                            </form>
                            <button  form="delete-request" type="submit" style="padding: 0px 6px;" class="btn btn-primary cancel-btn" title="Delete">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button></div>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
    @else
        <div class="text-center" style="margin-top: 50px;">
            <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
            <span class="fa-2x" style="margin-left: 10px;">No Quote Requests For You!</span>
        </div>
    @endif
</div>
<div class="text-center">
    {{ $userrequests->links() }}
</div>
    @endsection
@section('scripts')
    <script type="text/javascript">
        $('#sent_quotation').addClass('active')
    </script>
@endsection