<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/27/17
 * Time: 3:54 AM
 */
?>
@extends('layouts.account')
@section('title')
    {{ Auth::user()->name }} | Quotations
@endsection

@section('qtcontent')

<div class="content">
    @if(count($quotations) > 0)
    <div class="panel panel-default">
        <div class="panel-heading" style="background: #f5f5f5; color: #636b6f; font-weight: bolder; font-size: 1.2em;">
            Account \ Quotation
        </div>
        <ul class="list-group">
            <li class="list-group-item row" style="margin-right: 0px; margin-left: 0px; font-weight: 800;">
                <div class="col-md-1" style="padding: 0;">ID</div>
                <div class="col-md-2">From</div>
                <div class="col-md-2">Date</div>
                <div class="col-md-2">Products Total</div>
                <div class="col-md-2">Services Total</div>
                <div class="col-md-2">Total Quot</div>
                <div class="col-md-1">Action</div>
            </li>
            @foreach($quotations as $quotation)
                @if( $quotation->quotrdelete == false )
                <li class="list-group-item row" style="margin-right: 0px; margin-left: 0px;">
                    <div class="col-md-1">{{ $quotation->id }}</div>
                    <div class="col-md-2"><a href="{{ route('get.users',['user_id'=>$quotation->from_id])  }}">{{ $quotation->from }}</a></div>
                    <div class="col-md-2">{{ $quotation->created_at }}</div>
                    <div class="col-md-2 text-center">{{ $quotation->totalproducts ? $quotation->totalproducts : '-' }}</div>
                    <div class="col-md-2 text-center">{{ $quotation->totalservices ? $quotation->totalservices : '-'}}</div>
                    <div class="col-md-2">{{ $quotation->totalprice }}</div>
                    <div class="col-md-1">
                        <form id="delete-quotation" action="" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $quotation->id }}">
                        </form>
                        <button  form="delete-quotation" type="submit" style="padding: 0px 6px;" class="btn btn-primary cancel-btn" title="Delete">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </button>
                    </div>
                </li>
                @endif
            @endforeach
        </ul>
    </div>

    @else
        <div class="text-center" style="margin-top: 50px;">
            <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
            <span class="fa-2x" style="margin-left: 10px;">You Haven't Made Any Quotations Yet!</span>
        </div>
    @endif
</div>
<div class="text-center">
    {{ $quotations->links() }}
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#received_quotation').addClass('active')
    </script>
@endsection