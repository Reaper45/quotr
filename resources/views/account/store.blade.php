<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/27/17
 * Time: 3:53 AM
 */
?>

@extends('layouts.account')
@section('title')
    {{ Auth::user()->name }} | Store
@endsection

@section('qtcontent')

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Products</a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Services</a></li>
        @if($user->id == Auth::user()->id)
        <li class="pull-right">
            <button  style="margin-right: 26px;"  type="button" data-toggle="modal" data-target="#newPost" class="btn btn-primary my-btn my-btn-blue" id="addnew-btn">
                <i class="fa fa-plus" aria-hidden="true"></i>
                New
            </button>
        </li>
            @endif
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            @include('includes.product')
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
            @include('includes.service')
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#user_store').addClass('active')
    </script>
@endsection
