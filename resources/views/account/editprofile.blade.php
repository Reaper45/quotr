<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 3/27/17
 * Time: 3:34 AM
 */
?>
@extends('layouts.account')
@section('title')
    {{ Auth::user()->name }} | Edit Profile
@endsection

@section('qtcontent')

<div class="tab-title">
    Account \ Edit Profile
    <button class="btn btn-danger cancel-btn pull-right" onclick="delUser()" style="margin-top: -8px; margin-right: 15px;">
        <i class="fa fa-trash-o" aria-hidden="true" style="margin-right: 5px !important;"></i>Delete Account</button>
    <script type="text/javascript">
        var delUser = function () {
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this account!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function(){
                    document.getElementById('delete-account-form').submit();
                });
        }
    </script>
    <form id="delete-account-form" action="{{ route('delete.user') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
    </form>
</div>

<div class="row" style="margin-top: 20px">
    <div class="col-md-8" style="padding-bottom: 50px;">
        <form action="/" method="post" id="updateProfileForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Company Name</label>
                <input type="text" id="name" name="name" class="form-control" value="{{$user->name}}">
                <span class="help-block">
                    <strong id="nameError"></strong>
                </span>
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" class="form-control" value="{{$user->email}}">
                <span class="help-block">
                    <strong id="emailError"></strong>
                </span>
            </div>

            <div class="form-group">
                <label for="phone">Phone</label>
                <input type="tel" id="phone" name="phone" class="form-control" value="{{$user->phone}}">
                <span class="help-block">
                    <strong id="phoneError"></strong>
                </span>
            </div>
            <div class="form-group">
                {{--<label for="category">Category</label>--}}
                @include('includes.category')
            </div>

            {{--<div class="form-group">--}}
                {{--<label for="location">Location</label>--}}
                {{--<input type="text" id="location" name="location" class="form-control" value="{{ $user->location }}">--}}
            {{--</div>--}}

            <div class="form-group">
                @include('includes.locations')
            </div>

            <div class="form-group">
                <label for="bio">Bio</label><span class="pull-right">(max: 160 characters)</span>
                <textarea type="text" id="bio"  name="bio" class="form-control" placeholder="Write a short description of your company">{{ $user->bio }}</textarea>
            </div>


            <div class="form-group">
                <label for="website">Website</label>
                <input type="url" id="website" name="website" class="form-control" value="{{ $user->website }}">
            </div>

            <div class="row">
            <div class="form-group col-lg-6">
                <label for="facebook">Facebook</label>
                <input type="url" id="facebook" name="facebook" class="form-control" value="{{ $user->facebook }}">
            </div>

            <div class="form-group col-lg-6">
                <label for="twitter">Twitter</label>
                <input type="url" id="twitter" name="twitter" class="form-control" value="{{ $user->twitter }}">
            </div>
            </div>
        </form>
    </div>

    <div class="col-md-4" style="margin-top: 10px">
        <div>
            @if(Storage::disk('userlogo')->has($user->logo))
                <img id="profilepic" class="img-thumbnail img-responsive" src="{{ route('user.image', ['filename'=> $user->logo, '_token' =>csrf_token()]) }}">
            @else
                <img id="profilepic" class="img-thumbnail img-responsive" src="{{ asset('/img/wireframe.png') }}">
            @endif
        </div>
        <div class="item">

            <input name="image" type="file" form="updateProfileForm" id="file" class="inputfile inputfile-1"/>
            <label for="file" class="btn btn-primary upload-img">
                <i class="fa fa-upload" aria-hidden="true"></i>
                <span>Upload New Image &hellip;</span>
            </label>
        </div>
    </div>
</div>
<div class="row" >
    <div  class="col-md-8 control-btns" style=" border-bottom: 1px solid #ccc; padding-bottom: 20px;">
    <div class="pull-right" >
        <button type="reset" form="updateProfileForm" class="btn btn-default my-btn" style="background:#cccccc; ">
            <i class="fa fa-ban" aria-hidden="true"></i>
            Cancel
        </button>
        <button type="submit" form="updateProfileForm" class="btn btn-primary my-btn my-btn-blue">
            <i class="fa fa-floppy-o" aria-hidden="true"></i>
            Save Changes
        </button>
    </div>
    </div>
    <div class="col-md-4"></div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#user_editprofile').addClass('active')
    </script>
@endsection