@extends('layouts.app')
@section('title')
    {{ config('app.name')}} | Login
    @endsection

@section('style')
    <style type="text/css">
        .navbar-default{
            border: none;
            /*background: rgb(38, 180, 236);*/
        }

        .navbar{
            border: none;
        }
        .my-btn-blue{
            background: rgb(38, 180, 236);
            /*border-radius: 5px;*/
        }
        .app-footer{
            margin-top: -70px;
        }

    </style>
    @endsection

@section('content')
<div class="container auth-container" >
    {{--@include('includes.messageblock')--}}
    <div class="row" style="margin-right: 0px;">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-6" style="margin: 200px; color: #fff;">
                <div class="content">
                    <div class="">
                        <div class="home-title">Get Quotations,</br>
                        Send Quotations</div>
                        <div class="row">
                            <div class="col-md-2" style="padding: 0; margin-top: 10px;  ">
                                <span class="horizontal-line"></span>
                            </div>
                            <div class="col-md-10">
                                <p style="color: #E0F2FB;">Quotr lets companies and individuals request, send and receive Quotations.</p>
                            </div>
                        </div>
                    </div>

                    <a class="btn btn-default my-btn my-btn-blue" href="{{ route('register') }}" style="margin-top: 20px;">Get Started</a>
                </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row " style="margin-top: 140px;">
                <div class="col-md-6 col-md-offset-4">
                    <img class="img-responsive circle-logo center-block" src="{{ asset('img/logo1.png') }}" >
                </div>
                <form class="form-horizontal login-form" role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label"></label>

                        <div class="col-md-6 input-group">
                            <span class="input-group-btn input-group-addon">
                                <i class="fa fa-at" aria-hidden="true" style="font-size: initial;"></i>
                            </span>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required autofocus>

                        </div>
                        <div class="col-md-6 col-md-offset-4">
                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label"></label>

                        <div class="col-md-6 input-group">
                            <span class="input-group-btn input-group-addon">
                                <i class="fa fa-key" aria-hidden="true" style="font-size: initial;"></i>
                            </span>
                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                        </div>
                        <div class="col-md-6 col-md-offset-4">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>r
                        @endif
                        </div>
                        </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary my-btn my-btn-blue">
                                <i class="fa fa-unlock" aria-hidden="true"></i>
                                Login
                            </button>

                            <a style="color: #E0F2FB; margin-top: 20px;" class="btn btn-link" href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
