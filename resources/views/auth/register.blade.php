@extends('layouts.app')

@section('title')
    {{ config('app.name')}} | Register
@endsection

@section('style')
    <style type="text/css">
        .app-footer{
            margin-top: -70px;
        }
        .row{
            margin-right: 0px;
            margin-left: 0px;
        }

    </style>
@endsection
@section('content')
<div class="container auth-container register" style="height: 620px;">
    {{--@include('includes.messageblock')--}}
    <div class="row" style="margin-top: 100px;">
        <div class="col-md-8 col-md-offset-2">
            {{--<div class="panel panel-default" style="margin-top: 50px;">--}}
                {{--<div class="panel-heading">Register</div>--}}
                {{--<div class="panel-body">--}}
                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="api_token" value="{{ csrf_token() }}">

                        {{--Business Name--}}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Business Name:</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--Email--}}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--Phone No--}}
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone: </label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--Company Logo--}}
                        {{--<div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">--}}
                            {{--<label for="phone" class="col-md-4 control-label">Company Logo: </label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="logo" type="file" class="form-control" name="logo" value="{{ old('logo') }}" required autofocus>--}}

                                {{--@if ($errors->has('logo'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('logo') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary my-btn my-btn-blue">
                                    Register
                                </button>
                            </div>

                        </div>
                        <div class="col-md-6 col-md-offset-4">
                        <span class="" style="color: #E0F2FB; margin-top: 8px;">Already have an account?<strong><a style="color: #FFFFFF;" href="{{ route('login') }}">&nbsp;Log In.</a></strong></span>
                        </div>
                    </form>
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
@endsection
