<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 4/22/17
 * Time: 7:17 PM
 */
?>

@extends('layouts.app')
@section('title')
    {{ config('app.name') }} | Send-To
@endsection

@section('style')
    <style>
        .img-responsive{
            height: 100px;
        }
        .card{
            padding: 0px 15px 0px 0px;
        }
        .col-md-5{
            padding: 0;
        }
        .col-md-7{
            padding-top: 15px;
        }
        .app-footer{
            color: #636b6f !important;
        }
        .breadcrumb {
            padding: 10px 15px;
        }

    </style>
    <script type="text/javascript">

    </script>
@endsection


@section('content')
    <div class="container" style="margin-bottom: 60px;">
        @include('includes.messageblock')
        <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li><a href="/account">{{ Auth::user()->name }} </a></li>
            <li>Send To</li>
            <li class="search pull-right">
                <form action="{{ route('search.business')}}" method="post">
                    {{ csrf_field() }}
                    <input class="form-control" name="search" type="search" placeholder="Search Business" required>
                </form>
            </li>
        </ol>
        <div class="row">
            @foreach( $businesses->chunk(4) as $item)
                @foreach( $item as $business)
                    @if( $business->id != Auth::user()->id)
                    <div class="col-md-4 col-sm-6">
                        <a href="{{ route('quotation.send.to', ['name'=>$business->name ]) }}">
                        <div class="card row">
                            <div class="col-md-5">
                            @if(Storage::disk('userlogo')->has($business->logo))
                                <img class="img-responsive" src="{{ route('user.image', ['filename'=> $business->logo, '_token' =>csrf_token()]) }}">
                            @else
                                <img class="img-responsive company-logo" src="{{ asset('/img/wireframe.png') }}">
                            @endif
                            </div>
                            <div class="col-md-7">
                                <div >Name: {{ $business->name }}</div>
                                <div >Email: <span>{{ $business->email }}</span></div >
                            </div>
                        </div>
                        </a>
                    </div>
                    @endif
                @endforeach
            @endforeach
        </div>
        <div class="text-center">
            {{ $businesses->links() }}
        </div>

    </div>

@endsection
