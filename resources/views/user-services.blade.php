
@extends('layouts.myitems')
@section('tab')
<div role="tabpanel" class="tab-pane" id="sendServices">
    @if($services != null )
        @foreach($services->chunk(4) as $item)
            <div class="row">
                @foreach($item as $key => $service )
                    <div class="col-md-3" style="">
                        <div class="thumbnail">
                            @if(Storage::disk('serviceImages')->has($service->image))
                                <img id="click" src="{{ route('service.image', ['filename'=>$service->image]) }}">
                            @else
                                <img class="img-responsive" src="{{ URL::to('img/wireframe.png') }}" style="background: #fff;">
                            @endif
                            <div class="caption">
                                <label>{{ $service->name }}</label>
                                <div>
                                    <label>Ksh. {{ $service->price }}</label>
                                    @if( Session::get('send-cart')['services'] != null && array_key_exists($service->id, Session::get('send-cart')['services']))
                                        <button class="btn btn-default pull-right">Added</button>
                                    @else
                                        <a href="{{ route('add.quotation.send',['id'=>$service->id]) }}" type="submit" class="btn btn-default pull-right">Add</a>
                                    @endif
                                    <span  class="dropdown">
                                        {{--<a class="dropdown-toggle pull-right" data-toggle="dropdown" role="button" aria-expanded="false" href="#"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>--}}
                                        <ul class="dropdown-menu" role="menu" style="min-width: 130px;">
                                        <input id="id" type="hidden" value="{{ $service->id }}">
                                        <input id="name" type="hidden" value="{{ $service->name }}">
                                        <input id="price" type="hidden" value="{{ $service->price }}">
                                        <input id="desc" type="hidden" value="{{ $service->description }}">
                                        {{--<li><a id="edit-service" class="btn edit-service" data-toggle="modal" data-target="#editService" style="text-align: left;">Edit</a></li>--}}
                                        {{--<li><a href="{{ route('service.delete', ['id'=> $service->id]) }}">Delete</a></li>--}}
                                    </ul>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="text-center">
                {{ $services->links() }}
            </div>
        @endforeach
    @endif
</div>

@endsection
@section('scripts')
    <script type="text/javascript">
        $('#serviceslist').addClass('active')
    </script>
    @endsection