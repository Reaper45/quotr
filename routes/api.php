<?php

use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
 * Register new company and LogIn
 */
Route::group(['middleware'=>'auth:client'], function (){

    Route::post('/register', [
        'uses' => 'ApiController@UserRegistration'
    ]);

    Route::post('/login', [
        'uses' => 'ApiController@UserLogin'
    ]);
});


/**
 * API requests
 */

Route::group(['middleware'=>'auth:api'], function (){

    Route::get('/users/', [
        'uses'=>'ApiController@fetchUsers'
    ]);

    Route::get('/company/{company_id?}', [
        'uses' => 'ApiController@fetchCompany'
    ]);

    //returns combined products and services
    Route::get('/company/{company_id}/all-items/', [
        'uses' => 'ApiController@fetchCompanyBothPandS'
    ]);

    Route::get('/company/{company_id}/products/', [
        'uses' => 'ApiController@fetchCompanyProducts'
    ]);

    Route::get('/company/{company_id}/services/', [
        'uses' => 'ApiController@fetchCompanyServices'
    ]);

    Route::get('/products/{product_id}', [
        'uses' => 'ApiController@fetchProduct'
    ]);

    Route::get('/services/{service_id}', [
        'uses' =>'ApiController@fetchService'
    ]);

    Route::get('/categories/', [
        'uses'=>'ApiController@fetchCategories'
    ]);

    Route::get('/categories/{category_id}', [
        'uses'=>'ApiController@fetchBusinessUnderCategory'
    ]);

    Route::post('/updateprofile', [
        'uses' => 'ApiController@updateUserAccount',
    ]);

    Route::get('/get-history', [
        'uses'=> 'ApiController@getHistory'
    ]);

    // New
    Route::post('/checkout/', [
        'uses'=> 'ApiController@getCheckout',
    ]);

    Route::delete('/delete-account', [
        'uses' => 'ApiController@deleteUserAccount',
    ]);

    Route::delete('delete-product/{id}', [
        'uses' => 'ApiController@deleteProduct',
    ]);

    Route::delete('delete-service/{id}', [
        'uses' => 'ApiController@deleteService',
    ]);

    Route::delete('/delete-quotation/{id}', [
        'uses'=> 'ApiController@deleteQuotation',
    ]);

    Route::delete('/delete-sent-quotation/{id}', [
        'uses'=> 'ApiController@deleteSentQuotation',
    ]);

    Route::post('/send-quotation/complete', [
        'uses' => 'ApiController@sendQuotationComplete',
    ]);

});

/**
 * Adding new Product, Service
 */
Route::group(['middleware'=>'auth:api'], function (){

    Route::post('/products', [
        'uses' => 'ProductController@postProduct'
    ]);

    Route::post('/services', [
        'uses' => 'ServiceController@postService'
    ]);

    Route::post('/addavatar', [
        'uses' => 'ApiController@addAvatar'
    ]);

    Route::post('/addimage', [
        'uses' => 'ApiController@addImage'
    ]);

});

Route::post('/categories', [
    'uses'=>'CategoryController@postCategory'
]);

Route::get('/getimage/{filename}', [
    'uses'=>'UserController@getImage',
    'as'=>'user.image'
]);


Route::get('/get-service-image/{filename}', [
    'uses'=>'ServiceController@getServiceImage',
]);


Route::get('/get-product-image/{filename}', [
    'uses'=>'ProductController@getProductImage',
]);

// New
Route::post('/categories/icon', [
    'uses'=>'CategoryController@updateCategory'
]);

Route::get('/categories/icon/{filename}', [
    'uses'=>'CategoryController@getCategoryIcon'
]);

//Mobile Send Preview route.
Route::post('mobile/quotation-preview', [
    'uses'=> 'ApiController@mobileSendQuotationPreview'
]);

//Mobile Get Preview route.
Route::post('mobile/quotation-preview', [
    'uses'=> 'ApiController@mobileGetQuotationPreview'
]);

// Checkout from mobile ()
Route::get('/send-quotation/complete', [
    'uses' => 'ApiController@mobileQuotationComplete',
    'as' => 'mobile.quotation.finish',
]);
