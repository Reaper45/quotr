<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Apps;
use App\Http\Requests\ApiFormRequest;
use App\Mail\MailQuote;
use Illuminate\Support\Facades\Mail;

Auth::routes();

Route::get('/', function () {
        if (Auth::guest()){
            return view('welcome');
        }else{
            return redirect('/home');
        }
    });

Route::group(['middleware'=>'auth:web'], function(){

    Route::get('/home', 'HomeController@index');

    Route::get('/users/{user_id}', [
        'uses' => 'UserController@getUserProfile',
        'as' => 'get.users',
    ]);

    Route::get('/users/{user_id}/product/{id}', [
        'uses' => 'ProductController@getProductDetails',
        'as' => 'product.details'
    ]);

    Route::group(['prefix'=>'user'], function () {

        Route::get('account', [
            'uses' => 'UserController@getUserAccount',
            'as' => 'auth.account',
        ]);

        Route::get('account/quotations', [
            'uses' => 'UserController@getUserQuotations',
            'as' => 'user.quotations',
        ]);

        Route::get('account/requests', [
            'uses' => 'UserController@getUserRequests',
            'as' => 'user.requests',
        ]);

        Route::get('account/editprofile', [
            'uses' => 'UserController@getUserEditProfile',
            'as' => 'user.editprofile',
        ]);

        Route::post('/account', [
            'uses' => 'UserController@updateUserAccount',
            'as' => 'change.account'
        ]);

        Route::post('/delete-account', [
            'uses' => 'UserController@deleteUserAccount',
            'as' => 'delete.user',
        ]);

        Route::get('/getimage/{filename}', [
            'uses' => 'UserController@getImage',
            'as' => 'user.image'
        ]);


        Route::get('/{user_id}/service/{id}', [
            'uses' => 'ServiceController@getServiceDetails',
            'as' => 'service.details'
        ]);

        Route::get('/send-quotation', [
            'uses' => 'ProductController@sendQuotationProducts',
            'as' => 'quotation.send',
            'middleware' => 'auth'
        ]);

        Route::get('/send-quotation/services', [
            'uses' => 'ServiceController@sendQuotationServices',
            'as' => 'quotation.send.services',
            'middleware' => 'auth'
        ]);

        // Preview Quotation to send
        Route::get('/send-quotation/send/{name}', [
            'uses' => 'QuotationController@sendQuotationPreview',
            'as' => 'quotation.send.to',
        ]);

        // Get Quotation Preview
        Route::get('/get-quotation', [
            'uses' => 'QuotationController@getQuotationPreview',
            'as' => 'quotation.get',
        ]);

        // Checkout from web (Send Quotation)
        Route::get('/send-quotation/complete', [
            'uses' => 'UserController@sendQuoteFinal',
            'as' => 'send.quotation.finish',
        ]);

        // Checkout from web ( Get Quotation)
        Route::get('/get-quotation/complete', [
            'uses' => 'UserController@getQuoteFinal',
            'as' => 'get.quotation.finish',
        ]);
    });

    Route::get('/categories', [
        'uses'=>'UserController@fetchCategoriesFromDB',
        'as'=>'get.categories',
        'middleware'=>'reg'

    ]);

    Route::get('/categories/{id}', [
        'uses'=>'UserController@fetchCategoriesBusiness',
        'as'=>'get.businesses',
    ]);

    // PRODUCTS
    Route::post('/products', [
        'uses' => 'ProductController@postProduct',
        'as' => 'new.product'
    ]);

    Route::get('/product-image/{filename}', [
        'uses' => 'ProductController@getProductImage',
        'middleware' => 'auth',
        'as' => 'product.image'
    ]);

    Route::get('delete-product/{id}', [
        'uses' => 'ProductController@deleteProduct',
        'as' => 'product.delete'
    ]);

    Route::post('/update-product', [
        'uses' => 'ProductController@updateProduct',
        'as' => 'product.update'
    ]);

    //SERVICES
    Route::post('/service', [
        'uses' => 'ServiceController@postService',
        'as' => 'new.service'
    ]);

    Route::get('/service-image/{filename}', [
        'uses' => 'ServiceController@getServiceImage',
        'middleware' => 'auth',
        'as' => 'service.image'
    ]);

    Route::get('delete-service/{id}', [
        'uses' => 'ServiceController@deleteService',
        'as' => 'service.delete'
    ]);

    Route::post('/update-service', [
        'uses' => 'ServiceController@updateService',
        'as' => 'service.update'
    ]);

    Route::get('/add-service-to-cart/{id}', [
        'uses'=> 'ServiceController@addToCart',
        'as' => 'add.service.to.cart'
    ]);

    Route::get('/remove-service-from-cart/{id}', [
        'uses'=> 'ServiceController@removeFromCart',
        'as' => 'cart.remove.service'
    ]);

    Route::post('/add-product-to-cart/{id}', [
        'uses'=> 'ProductController@addToCart',
        'as' => 'add.product.to.cart'
    ]);

    Route::get('/remove-product-from-cart/{id}', [
        'uses'=> 'ProductController@removeFromCart',
        'as' => 'cart.remove.product'
    ]);

    Route::get('/edit-cart', [
        'uses'=> 'QuotationController@editCart',
        'as' => 'cart.edit'
    ]);

    Route::get('/empty-cart', [
        'uses'=> 'QuotationController@emptyCart',
        'as' => 'cart.empty'
    ]);

    Route::post('/delete-quotation', [
        'uses'=> 'QuotationController@deleteQuotation',
        'as' => 'delete.quotation'
    ]);

    Route::post('/delete-sent-quotation', [
        'uses'=> 'QuotationController@deleteSentQuotation',
        'as' => 'delete.request'
    ]);

    Route::get('/quotation-made-to/{id}', [
        'uses'=> 'QuotationController@quotationMadeTo',
        'as' => 'quotation.made.to'
    ]);

    // Route::get('/checkout', [
    //     'uses'=> 'UserController@getCheckout',
    //     'as' => 'user.checkout'
    // ]);

    Route::post('add-send-quotation/{id}',[
        'uses'=> 'ProductController@addToQuotation',
        'as'=>'add.quotation.send',
    ]);

    Route::get('add-send-quotation/{id}',[
        'uses'=> 'ServiceController@addToQuotation',
        'as'=>'add.quotation.send',
    ]);

    Route::get('empty-send-quotation/',[
        'uses'=> 'QuotationController@emptysendCart',
        'as'=>'empty.quotation.send',
    ]);

    Route::get('/remove-send-service/{id}', [
        'uses'=> 'ServiceController@removeFromSendCart',
        'as' => 'remove.send.service'
    ]);

    Route::get('/remove-send-product/{id}', [
        'uses'=> 'ProductController@removeFromSendCart',
        'as' => 'remove.send.product'
    ]);


    Route::get('user/send-quotation/send', [
        'uses'=> 'UserController@getSendToBusinesses',
        'as' => 'send.to'
    ]);

    // search business
    Route::post('user/send-quotation/search-business', [
        'uses'=> 'UserController@searchBusiness',
        'as' => 'search.business'
    ]);

    Route::get('user/send-quotation/dd', function (){
        dd(Session::get('send-cart'));
    });

    // Testing one, two
    Route::get('/sendmail', function (){
//        dd(Session::get('cart'));
        if(Session::has('cart')) {
            Mail::to('jomwashighadi@gmail.com')->queue(new MailQuote());
            return response(['message' => 'Request completed']);
        }
        return response(['empty'=>'cart']);
    });

});

Route::get('/createapp', function (){
    return view('createapp');
})->name('createapp');


Route::post('/createapp', function (ApiFormRequest $request){

    $name = $request['name'];
    $api_token = $request['_token'];

    $app = new Apps();
    $app->name = $name;
    $app->api_token = $api_token;

    if($app->save()){
        $myapp = array($name => $api_token);
        return redirect('/createapp')->with(['myapp' => $myapp]);
    }
})->name('createapp');




Route::get('/quotation-temp', function (Request $request){
    $cart = Session::get('send-cart');

    dd($cart);
    $cart['to'] = Auth::user();
    $cart['from'] = App\User::find($cart['quoteTo'])->first();

   return view('emails.quotation')->with(['cart'=>$cart]);
});
