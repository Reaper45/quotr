

## About Quotr

Get easily any quotations you want.


## License

This software is developed with Laravel framework, which is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
