<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'logo', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function app()
    {
        $this->hasMany('App\Apps');
    }

    public function product()
    {
        return $this->hasMany('App\Products');
    }

    public function service()
    {
        return $this->hasMany('App\Services');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function quotation()
    {
        return $this->hasMany('App\Quotation');
    }

    public function sentquotation()
    {
        return $this->hasMany('App\SentQuotation');
    }
}
