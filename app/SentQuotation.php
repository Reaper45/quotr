<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SentQuotation extends Model
{
    protected $fillable = ['totalproducts', 'totalservices', 'totalprice'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
