<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MailQuote extends Mailable
{
    use Queueable, SerializesModels;

    public $cart;
    public $quoteTo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($cart)
    {
        $this->cart = $cart;
        // dd($this->cart);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->cart['from'])
                    ->subject('A Quotr Quotation')
                    ->view('emails.quotation')->with(['cart'=>$this->cart]);
    }
}
