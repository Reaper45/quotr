<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function postCategory(Request $request)
    {
        $validator = Validator($request->all(), [
            'name' => 'required'
        ]);

        //dd($request->all());

        if(!$validator->fails()){
            $icon = Input::file('icon');

            $name = $request['name'];

            $iconname = str_slug($name, '_');

            $category = new Category();

            //check if icon uploaded
            if(isset($icon)){
                if ($icon->isValid()) {
                    $extension = $icon->getClientOriginalExtension();
                    $fileName = $iconname.'.'.$extension;
                    $content = File::get($icon);

                    if (Storage::disk('categoryicon')->put($fileName, $content)) {
                        $category->icon  = $fileName;
                        $category->name = $name; //set name
                        if($category->save()){
                            $message = ['success'=> 'Industry added successfully'];
                            return $message;
                        }
                    }
                }
                return response(['error'=>'Invalid File!']);
            }

            //Save even without the icon
            $category->name = $request->get('name'); //set name
            if ($category->save()) {
                $message = ['success'=> ''.$category->name.' industry added successfully without icon'];
                            return $message;
            }
        }
        $errors = $validator->errors();
        return $errors;
    }

    public function getCategories($category_id = null)
    {
        if(isset($category_id)){
            return Category::where('id', '=', $category_id)->get()->first();
        }
        return Category::all();
    }

    public function fetchBusinessUnderCategory($category_id)
    {
        return User::where('category_id', '=', $category_id)->get();
    }

    public function updateCategory(Request $request)
    {
        $id = $request['id'];
        $icon = Input::file('icon');

        $category = Category::find($id);
        if(isset($icon)){
            if ($icon->isValid()) {
                $extension = $icon->getClientOriginalExtension();
                $fileName = $id.'.'.$extension;
                $content = File::get($icon);

                if (Storage::disk('categoryicon')->put($fileName, $content)) {
                    $category->icon  = $fileName;
                    $category->update();
                    return $category;
                }
            }
            return response(['error'=>'Invalid File!']);
        }
    }

    public function getCategoryIcon($filename)
    {
        if (Storage::disk('categoryicon')->has($filename)){
            $file = Storage::disk('categoryicon')->get($filename);
            return response($file, 200);
        }
    }

}
