<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\MailQuote;
use App\OrderedProducts;
use App\OrderedServices;
use App\Products;
use App\Quotation;
use App\SentQuotation;
use App\Services;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function updateUserAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'bail|required|email',
            'phone' => 'required',
            'bio' => 'max:160'
        ]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return response(['error' => true, 'message' => $error], 403);
        }

        $user = Auth::user();

        $user->name     = $request['name'];
        $user->email    = $request['email'];
        $user->phone    = $request['phone'];
        $user->location = $request['location'];
        $user->category_id = $request['category'];
        $user->bio      = $request['bio'];
        $user->website  = $request['website'];
        $user->facebook = $request['facebook'];
        $user->twitter  = $request['twitter'];

        $user = Auth::user();

        $inputImage = Input::file('image'); // Get image from form input

        if(isset($inputImage)){
            if ($inputImage->isValid()) { //  verify that there were no problems uploading the file
                $extension = $inputImage->getClientOriginalExtension(); // Get image extension type(jpeg, jpg, png ...)
                $fileName = $user->id.'.'.$extension; // Replace with how you want to save image name in db
                $content = File::get($inputImage);

                if (Storage::disk('userlogo')->put($fileName, $content)) {
                    $user->logo  = $fileName;
                }
            }
        }

        if(!$user->update()){
            $error = ['message => Error Saving user details!'];
            return response(['error'=> true, $error]);
        }
        return response($user);
    }

    public function fetchUsersFromDB($user_id = null)
    {
        if(isset($user_id)){
            if(!User::find($user_id)){
                return response(404);
            }
            return User::where('id', '=', $user_id)->get()->first();
        }
        $user = User::all();

        return $user;
    }

    public function getUserAccount()
    {
        $user_id = Auth::user()->id;

        $products = Products::orderby('created_at', 'desc')->where('user_id', '=', $user_id)->paginate(12);
        $services = Services::orderby('created_at', 'desc')->where('user_id', '=', $user_id)->paginate(12);

        return view('account.store', ['user'=> Auth::user(),
            'products'=> $products,
            'services' => $services]);
    }

    public function getUserQuotations()
    {
        $user_id = Auth::user()->id;

        $quotations = DB::table('quotations')->where([
            ['user_id', '=', $user_id],
            ['quotrdelete', '=', false],
        ])->paginate(10);

        foreach ($quotations as $quotation){
            $quotation->from_id = $quotation->from;
            $quotation->from = User::find($quotation->from)->name;
        }

        return view('account.recieved-quotations', ['user'=> Auth::user(),'quotations' => $quotations]);
    }

    public function getUserRequests()
    {
        $user_id = Auth::user()->id;

        $sentquotations = DB::table('sent_quotations')->where([
            ['user_id', '=', $user_id],
            ['quotrdelete', '=', false],
        ])->paginate(10);

        foreach ($sentquotations as $sentquotation){
            $sentquotation->madeto_id = $sentquotation->madeto;
            $sentquotation->madeto =  User::find($sentquotation->madeto)->name;
        }

        return view('account.sent-quotations', ['user'=> Auth::user(),'userrequests' => $sentquotations]);
    }

    public function getUserEditProfile()
    {
        $categories = Category::all();
        return view('account.editprofile', ['user'=> Auth::user(),'categories' => $categories]);

    }

    public function getUserProfile($user_id)
    {
        $user = User::find($user_id);
        if($user && $user_id != Auth::user()->id){
            $products   = Products::orderby('created_at', 'desc')->where('user_id', '=', $user_id)->paginate(12);
            $services   = Services::orderby('created_at', 'desc')->where('user_id', '=', $user_id)->paginate(12);

            return view('profile')->with(['user'=> $user, 'products'=> $products, 'services' => $services]);

        }
        return view('profile')->withErrors(['error'=>'User Not Found!']);
    }

    public function deleteUserAccount(Request $request)
    {
        if($request['user_id'] == Auth::user()->id){
            $user_id = $request['user_id'];

            Auth::logout();

            Products::where('user_id', '=', $user_id )->delete();
            Services::where('user_id', '=', $user_id )->delete();
            Quotation::where('user_id', '=', $user_id )->delete();

            // Delete using Laravel events
            // OrderedProducts::where()->delete();
            // OrderedServices::where()->delete();

            $user = User::find($user_id);
            $user->delete();

            return redirect('/');
        }
    }

    public function getImage($filename)
    {
        $file = Storage::disk('userlogo')->get($filename);
        return response($file, 200);
    }

    public function fetchCategoriesFromDB()
    {
        $categories = DB::table('category')->paginate(16);

        return view('categories', ['categories'=>$categories]);
    }

    public function fetchCategoriesBusiness($category_id)
    {
        $CategoryController = new CategoryController();

        $category = $CategoryController->getCategories($category_id);
        $users = $CategoryController->fetchBusinessUnderCategory($category_id);

        return view('businesses', ['users'=>$users, 'category'=>$category]);
    }

    public function quotationUsersName($id)
    {
        if(User::find($id)){
            $user = User::find($id);
            return $user->name;
        }
        return 'Quotr';
    }

    public function getSendToBusinesses()
    {
        if(Session::has('send-cart')) {
            $businesses = DB::table('users')->paginate(16);
            return view('send-to')->with(['businesses' => $businesses]);
        }
        return redirect()->back()->withErrors(['error'=>'Please Select Items First To Send!']);
    }

    public function searchBusiness(Request $request)
    {
        $q = $request['search'];
        $businesses = DB::table("users")->where("name", "LIKE", "%{$q}%")->orWhere("email", "LIKE", "%{$q}%")->paginate(16);
        // dd($businesses);

        if ($businesses->isNotEmpty()) {
            return view('send-to')->with(['businesses' => $businesses]);
        }
        Session::flash('info', 'Search did not match our Recods!');
        return redirect()->action('UserController@getSendToBusinesses');
        // $this->getSendToBusinesses();
    }

    // Complete send Quote
    public function sendQuoteFinal(Request $request)
    {
        if(Session::has('send-cart')) {
            $cart = Session::get('send-cart');

            $quoteTo = $cart['to'];

            // Restructuring the cart Object
            $cart['quoteTo'] = $quoteTo->id;

            $cart['to'] = $quoteTo;
            $cart['from'] = Auth::user();

            session(['mail-cart'=>$cart]);
            // dd($cart['from']);

            Mail::to($quoteTo)->send(new MailQuote($cart));

            $products = $cart['products'];
            $services = $cart['services'];

            $sentquotation = new SentQuotation();

            $sentquotation->totalproducts = $cart['products']['totalProducts'];
            $sentquotation->totalservices = $cart['services']['totalServices'];
            $sentquotation->totalprice = $cart['totalPrice'];
            $sentquotation->quotrdelete = false;
            $sentquotation->quoteedelete = false;
            $sentquotation->madeto = $cart['quoteTo'];

            $sentquotation = $request->user()->quotation()->save($sentquotation);

            if (!empty($products)) {
                foreach ($products as $key => $value) {
                    if ($key != 'totalProducts') {
                        $orderedProduct = new OrderedProducts();

                        $orderedProduct->product_id = $key;
                        $orderedProduct->quotation_id = $sentquotation->id;
                        $orderedProduct->qty = $value['qty'];

                        $orderedProduct->save();
                    }
                }
            }

            if (!empty($services)) {
                foreach ($services as $key => $value) {
                    if ($key != 'totalServices') {
                        $orderedService = new OrderedServices();

                        $orderedService->service_id = $key;
                        $orderedService->quotation_id = $sentquotation->id;
                        $orderedService->qty = $value['qty'];

                        $orderedService->save();
                    }
                }
            }
            $request->session()->forget('send-cart');
            $request->session()->forget('mail-cart');

            return redirect('user/account')->with(['success' => 'Thank You for Using QUOTR! The Quotation Has Been Forwarded To Your Email!']);
        // return redirect('quotation')->withErrors(['error'=>'Could NOT send Email! Please Retry']);
        }
        else{
            return redirect('categories')->withErrors(['error'=> 'Populate your quote cart first to get Quotation!']);
        }

    }

    // Complete Get Quote
    public function getQuoteFinal(Request $request)
    {
        if(Session::has('cart')) {
            $cart = Session::get('cart');

            $cart['to'] = Auth::user();
            $cart['from'] = User::find($cart['quoteTo'])->first();

            session(['mail-cart'=>$cart]);
            dd($cart);

            Mail::to(Auth::user())->send(new MailQuote($cart));

            $products = $cart['products'];
            $services = $cart['services'];

            $quotation = new Quotation();

            $quotation->totalproducts = $cart['products']['totalProducts'];
            $quotation->totalservices = $cart['services']['totalServices'];
            $quotation->totalprice = $cart['totalPrice'];
            $quotation->quotrdelete = false;
            $quotation->quoteedelete = false;
            $quotation->from = $cart['quoteTo'];

            $quotation = $request->user()->quotation()->save($quotation);

            if (!empty($products)) {
                foreach ($products as $key => $value) {
                    if ($key != 'totalProducts') {
                        $orderedProduct = new OrderedProducts();

                        $orderedProduct->product_id = $key;
                        $orderedProduct->quotation_id = $quotation->id;
                        $orderedProduct->qty = $value['qty'];

                        $orderedProduct->save();
                    }
                }
            }

            if (!empty($services)) {
                foreach ($services as $key => $value) {
                    if ($key != 'totalServices') {
                        $orderedService = new OrderedServices();

                        $orderedService->service_id = $key;
                        $orderedService->quotation_id = $quotation->id;
                        $orderedService->qty = $value['qty'];

                        $orderedService->save();
                    }
                }
            }
            $request->session()->forget('cart');
            $request->session()->forget('mail-cart');

            return redirect('user/account')->with(['success' => 'Thank You for Using QUOTR! The Quotation Has Been Forwarded To Your Email!']);
        // return redirect('quotation')->withErrors(['error'=>'Could NOT send Email! Please Retry']);
        }
        else{
            return redirect('categories')->withErrors(['error'=> 'Populate your quote cart first to get Quotation!']);
        }
    }
}