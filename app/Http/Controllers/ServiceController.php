<?php

namespace App\Http\Controllers;

use App\Services;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public $services = null;
    public $products = null;
    public $totalQty = 0;
    public $totalPrice = 0;
    public $quoteTo;

    public $send_products = null;
    public $send_services = null;
    public $send_totalQty = 0;
    public $send_totalPrice = 0;
    public $send_quoteTo;

    public function postService(Request $request)
    {
        $fileName = 'null';

        $validator = Validator::make($request->all(), [
            'name'        => 'required',
            'description' => 'required|max:120',
            'category'    => 'required',
            'image'       => 'required',
            'price'       => 'required',
        ]);

        if ($validator->fails()){
            $error = $validator->errors();

            return response(['errors' => $error], 403);
        }

        if (Input::file('image')->isValid()) {
            $file = Input::file('image');
            $extension = Input::file('image')->getClientOriginalExtension();

            $fileName = uniqid() . '.' . $extension;

            $content = File::get($file);

            if (Storage::disk('serviceImages')->put($fileName, $content)) {

                $service = new Services();

                $service->name = $request['name'];
                $service->description = $request['description'];
                $service->image = $fileName;
                $service->price = $request['price'];
                $service->qty = $request['qty'];

                if ($request->user()->service()->save($service)){
                    return response($service, 200);
                }
            }
        }
    }

    public function fetchServicesFromDB($service_id = null)
    {
        if(isset($service_id)){
            $service = Services::where('id', '=', $service_id)->get()->first();

            return $service;
        }
        $allService = Services::all();

        return $allService;
    }

    public function fetchCompanyServices($user_id)
    {
        $companyServices = Services::orderby('created_at', 'desc')->where('user_id', '=', $user_id)->get();

        return $companyServices;
    }

    public function getServiceDetails($user_id, $service_id)
    {
        $service = $this->fetchServicesFromDB($service_id);
        $services = Services::where('user_id', '=',$user_id)->where('id', '!=', $service_id)->paginate(12);

        return view('addtocart-service', ['service'=>$service, 'services'=>$services]);

    }

    public function  getServiceImage($filename){
        $file = Storage::disk('serviceImages')->get($filename);
        return new Response($file, 200);
    }

    public function deleteService($id){
        $service = Services::find($id);

        if(Auth::user() != $service->user)
        {
            return redirect()->back();
        }
        $service->delete();
        return redirect()->back()->with(['success'=>'Service Successfully Deleted!']);
    }

    public function updateService(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'price' => 'required',
            'description' => 'required|max:120'
        ]);

        if($validator->fails()){
            return response(['errors'=>$validator->errors()],403);
        }

        $service = Services::find($request['id']);

        $service->name = $request['name'];
        $service->price = $request['price'];
        $service->description = $request['description'];

        if($request['image'] != null) {
            if (Input::file('image')->isValid()) {
                $file = Input::file('image');
                $extension = Input::file('image')->getClientOriginalExtension();

                $fileName = uniqid() . '.' . $extension;

                $content = File::get($file);

                if (Storage::disk('serviceImages')->put($fileName, $content)) {
                    $service->image = $fileName;
                }
            }
        }
        if ($service->update())
            return response(['message'=>'Services Successfully Updated!'], 200);

        return response(['error'=> 'Could NOT save Changes. Please Retry!'],422);
    }

    public function addToCart(Request $request, $id)
    {
        $service = Services::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;

        $user = $service->user->id;

        if($oldCart){
            if ( $oldCart['quoteTo'] != $user ){
                return redirect()->route('get.users', $oldCart['quoteTo'] )->with(['info'=> 'Items MUST be from one Business! Empty Quote Cart first if you wish to Change Business']);
            }

            $this->products = $oldCart['products'];
            $this->totalQty = $oldCart['totalQty'];
            $this->totalPrice = $oldCart['totalPrice'];
            $this->services = $oldCart['services'];
            $this->quoteTo = $oldCart['quoteTo'];
        }
        $this->quoteTo = $user;

        $storedItem = ['qty' => 1, 'price' => $service->price, 'service'=>$service];
        if($this->services){
            if(array_key_exists($id, $this->services)){
                return redirect()->back()->with(['info'=>'Service Already Exists!']);
            }
        }

        $storedItem['price'] = $service->price * $storedItem['qty'];

        $this->services[$id] = $storedItem;
        $this->services['totalServices'] = 0 ;
        foreach ($this->services as $item){
            $this->services['totalServices'] += $item['price'];
        }

        $this->totalQty++;
        $this->totalPrice += $service->price;

        $cart = ['products'=>$this->products, 'services'=>$this->services, 'totalQty'=>$this->totalQty, 'totalPrice'=>$this->totalPrice, 'quoteTo'=>$this->quoteTo];

        $request->session()->put('cart', $cart);

//        dd(Session::get('cart'));
//        Session::forget('cart');

        Session::flash('success', 'Item Successfully Added to Cart!');
        return redirect()->back();
    }

    public function removeFromCart(Request $request, $id)
    {
        if(!Session::has('cart')){
            return redirect('/categories')->withErrors(['error'=>'Your Quote Cart Is Empty!']);
        }
        $oldCart = Session::get('cart');

        if($oldCart){
            $this->services = $oldCart['services'];
            $this->totalQty = $oldCart['totalQty'];
            $this->totalPrice = $oldCart['totalPrice'];
            $this->products = $oldCart['products'];
            $this->quoteTo = $oldCart['quoteTo'];
        }

        $service = $this->services[$id];
        if(!$service){
            return redirect()->back()->withErrors(['error'=>'Item Does NOT exist in Your Quote Cart!']);
        }
        $qty = $service['qty'];
        $price = $service['price'];

        $this->services['totalServices'] -= $price;
        $this->totalQty -= $qty;
        $this->totalPrice -= $price;

        unset($this->services[$id]);

        $cart = ['services'=>$this->services,
            'products'=>$this->products,
            'totalQty'=>$this->totalQty,
            'totalPrice'=>$this->totalPrice,
            'quoteTo'=> $this->quoteTo];

        $request->session()->put('cart', $cart);
        return redirect()->back()->with(['success'=> 'Item Removed From Quote Cart!']);
    }

    // Populating send quotation with services
    public function addToQuotation(Request $request, $id)
    {
        $service = Services::find($id);
        $oldCart = Session::has('send-cart') ? Session::get('send-cart') : null;

        if($oldCart){
            $this->send_products = $oldCart['products'];
            $this->send_totalQty = $oldCart['totalQty'];
            $this->send_totalPrice = $oldCart['totalPrice'];
            $this->send_services = $oldCart['services'];
            $this->send_quoteTo = $oldCart['quoteTo'];
        }

        $storedItem = ['qty' => 1, 'price' => $service->price, 'service'=>$service];
        if($this->send_services){
            if(array_key_exists($id, $this->send_services)){
                return redirect()->back()->with(['info'=>'Service Already Exists!']);
            }
        }

        $storedItem['price'] = $service->price * $storedItem['qty'];

        $this->send_services[$id] = $storedItem;
        $this->send_services['totalServices'] = 0 ;
        foreach ($this->send_services as $item){
            $this->send_services['totalServices'] += $item['price'];
        }

        $this->send_totalQty++;
        $this->send_totalPrice += $service->price;

        $cart = ['products'=>$this->send_products, 'services'=>$this->send_services, 'totalQty'=>$this->send_totalQty, 'totalPrice'=>$this->send_totalPrice, 'quoteTo'=>null];

        $request->session()->put('send-cart', $cart);

//        dd(Session::get('send-cart'));
//        Session::forget('cart');

        Session::flash('success', 'Item Successfully Added to Quote Cart!');
        return redirect()->back();

        /////////

    }

    public function sendQuotationServices(Request $request)
    {
        if(Auth::user()) {
            $services = Services::orderby('created_at', 'desc')->where('user_id', '=', Auth::user()->id)->paginate(12);

            return view('user-services')->with(['services'=>$services]);
        }
    }

    public function removeFromSendCart(Request $request, $id)
    {
        if(!Session::has('send-cart')){
            return redirect('user/send-quotation')->withErrors(['error'=>'Your Quote Cart Is Empty!']);
        }
        $oldCart = Session::get('send-cart');


        if($oldCart){
            $this->services = $oldCart['services'];
            $this->totalQty = $oldCart['totalQty'];
            $this->totalPrice = $oldCart['totalPrice'];
            $this->products = $oldCart['products'];
            $this->quoteTo = $oldCart['quoteTo'];
        }

        $service = $this->services[$id];
        if(!$service){
            return redirect()->back()->withErrors(['error'=>'Item Does NOT exist in Your Quote Cart!']);
        }
        $price = $service['price'];

        $this->services['totalServices'] -= $price;
        $this->totalQty --;
        $this->totalPrice -= $price;

        unset($this->services[$id]);

        $cart = ['services'=>$this->services,
            'products'=>$this->products,
            'totalQty'=>$this->totalQty,
            'totalPrice'=>$this->totalPrice,
            'quoteTo'=> $this->quoteTo];

        $request->session()->put('send-cart', $cart);
        return redirect()->back()->with(['success'=> 'Item Removed From Quote Cart!']);
    }
}
