<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $user = new UserController();
//
//        $ProductController  = new ProductController();
//        $ServiceController  = new ServiceController();
//        $CategoryController = new CategoryController();
//
//        $products   = $ProductController->fetchProductsFromDB();
//        $services   = $ServiceController->fetchServicesFromDB();
//        $categories = $CategoryController->getCategories();
//
//        $users = $user->fetchUsersFromDB();
        return view('home');
    }
}