<?php

namespace App\Http\Controllers;
/**
 * Controller to handle the API request. Could Implement functions api.php(routes) just wanted clean routes.
 */

use App\Category;
use App\Mail\MailQuote;
use App\OrderedProducts;
use App\OrderedServices;
use App\Products;
use App\Quotation;
use App\SentQuotation;
use App\Services;
use App\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\GenericUser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
//use Illuminate\Foundation\Auth\AuthenticatesUsers as Auth;

class ApiController extends Controller
{
    protected $redirectTo = null;

    use RegistersUsers;

    /**
     * Register user if email is not taken
     *
     * @param Request $request
     * @return Response
     *
     */
    public function deleteUserAccount(Request $request)
    {
        if($request['user_id'] != Auth::user()->id)
            return response(['error'=>true, 'message'=>'You Don\'t Have Permission To Perform this Action!'],403);
        $user_id = $request['user_id'];

        Products::where('user_id', '=', $user_id )->delete();
        Services::where('user_id', '=', $user_id )->delete();
        Quotation::where('user_id', '=', $user_id )->delete();

        // Delete using Laravel events
//            OrderedProducts::where()->delete();
//            OrderedServices::where()->delete();

        $user = User::find($user_id);
        $user->delete();

        return response(['error'=>false], 200);

    }

    public function UserRegistration(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'     => 'required',
            'email'    => 'bail|required|email|unique:users',
            'phone'    => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return response(['error'=>true, 'message'=>$error], 409);
        }

        $user = new User();

        $user->name      = $request->input('name');
        $user->email     = $request->input('email');
        $user->phone     = $request->input('phone');
        $user->password  = Hash::make($request->input('password'));
        $user->api_token = Hash::make($request->input('email'));

        if(!$user->save()){
            return response(['error'=>true,'message'=>'Error saving user details'], 500);
        }
        return response(['error'=>false, 'user'=>$user]);
    }

    public function UserLogIn(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if($validator->fails()) {
            $error = $validator->errors();
            return response($error);
        }

        $email    = $request['email'];
        $password = $request['password'];

        if(Auth::guard('web')->attempt(['email'=>$email,'password'=>$password])){
            $user = Auth::guard('web')->user();

            return response(['error' => false, 'user'=>$user]);
        }
        $error = ['error' => true, 'message'=>['failed'=>'These credentials do not match our records.']];
        return response($error, 404);
    }

    public function addAvatar(Request $request)
    {
        $encoded_str = $request['avatar'];

        // We need to remove the "data:image/png;base64,"
        // $base_to_php = explode(',', $encoded_str);
        //
        // $data = $base_to_php[0]; // "data:image/png;base64,"
        // $encoded_str = $base_to_php[1]; // Base64 data

        $decoded_str = base64_decode($encoded_str);

        // Get image extention
        // $split = explode('/', $data)[1];
        // $extension =  explode(';', $split)[0];

        $name = $request->user()->id.'.jpeg';

        if(Storage::disk('userlogo')->put($name, $decoded_str)){
            $user = $request->user();

            $user->logo = $name;
            if($user->update()){
                $res = ["error" =>false, "message"=>$name];
            }
            else{
                $res = ["error" =>true, "message"=>"Error Uploading Image"];
            }
            return response($res);
        }
    }

    // Uploading Image as file
    public function addImage(Request $request)
    {
      $user = Auth::user();
      $inputImage = Input::file('avatar'); // Get image from form input

      if(isset($inputImage)){
          if ($inputImage->isValid()) { //  verify that there were no problems uploading the file
              $extension = $inputImage->getClientOriginalExtension(); // Get image extension type(jpeg, jpg, png ...)
              $fileName = $user->id.'.'.$extension; // Replace with how you want to save image name in db
              $content = File::get($inputImage);

              if (Storage::disk('userlogo')->put($fileName, $content)) {
                  $user->logo  = $fileName;
                  if($user->update()){
                      $res = ["error" =>false, "message"=>$fileName];
                  }
                  else{
                      $res = ["error" =>true, "message"=>"Error Uploading Image"];
                  }
                  return response($res);
              }
          }
      }
      return response(["error" =>true, "message"=>"Unknown Error!"]);
    }

    public function fetchCompany($user_id = null)
    {
        if(isset($user_id)){
            if(!User::find($user_id)){
                $error = ["error" =>true, "message"=>"The User does not exist!"];
                return response($error,404);
            }
            $user = User::where('id', '=', $user_id)->get()->first();
            return response($user, 200);
        }
        $user = User::all();

        return  response($user, 200);
    }

    public function fetchCategories()
    {
        $CategoryController = new CategoryController();

        $categories = $CategoryController->getCategories();
        return response(['categories' => $categories]);
    }

    //Should fetch all users in the system
    public function fetchUsers(){
        $usercontroller = new UserController();
        $users = $usercontroller->fetchUsersFromDB();
        return response(['users' => $users]);
    }

    public function fetchBusinessUnderCategory($category_id)
    {
        $CategoryController = new CategoryController();

        $businesses  = $CategoryController->fetchBusinessUnderCategory($category_id);
        return response(['businesses'=>$businesses]);
    }

    public function fetchProduct($product_id)
    {
        $product = Products::where('id', '=', $product_id)->get();
        return response(['product'=>$product],200);
    }

    public function fetchService($service_id)
    {
        $service = Services::where('id', '=', $service_id)->get();
        return response(['service'=>$service],200);
    }

    //Fetch company products & services
    //together for sending as quotation
    public function fetchCompanyBothPandS($user_id)
    {
        $ProductController  = new ProductController();
        $ServiceController  = new ServiceController();

        $products = $ProductController->fetchCompanyProducts($user_id)->toArray();
        $services = $ServiceController->fetchCompanyServices($user_id)->toArray();


        return response(['all_items'=>array_merge($products, $services)], 200);
    }

    public function fetchCompanyProducts($user_id)
    {
        $ProductController  = new ProductController();
        $products = $ProductController->fetchCompanyProducts($user_id);

        return response(['products'=>$products], 200);
    }

    public function fetchCompanyServices($user_id)
    {
        $ServiceController  = new ServiceController();
        $services = $ServiceController->fetchCompanyServices($user_id);

        return response(['services'=>$services], 200);
    }

    public function updateUserAccount(Request $request)
    {
        $user = Auth::user();

        if(isset($request['name'])){
            $this->validate($request,[
                'name'=>'required'
            ]);
            $name = $request['name'];

            $user->name = $name;
            $res = ['error'=> false, 'name' => $name];
        }
        elseif (isset($request['category_id'])){
            $this->validate($request,[
                'category_id'=>'required'
            ]);
            $category_id = $request['category_id'];


            $user->category_id = $category_id;
            $res = ['error'=> false, 'category_id' => $category_id];
        }
        elseif (isset($request['bio'])){
            $bio = $request['bio'];

            $user->bio = $bio;
            $res = ['error'=> false, 'bio' => $bio];
        }
        elseif (isset($request['phone'])){
            $phone = $request['phone'];

            $user->phone = $phone;
            $res = ['error'=> false, 'phone' => $phone];
        }
        elseif (isset($request['location'])){
            $location = $request['location'];


            $user->location = $location;
            $res = ['error'=> false, 'location' => $location];
        }
        elseif (isset($request['website'])){
            $website = $request['website'];


            $user->website = $website;
            $res = ['error'=> false, 'website' => $website];
        }
        elseif (isset($request['facebook'])){
            $facebook = $request['facebook'];


            $user->facebook = $facebook;
            $res = ['error'=> false, 'facebook' => $facebook];
        }
        elseif (isset($request['twitter'])){
            $twitter = $request['twitter'];


            $user->twitter = $twitter;
            $res = ['error'=> false, 'twitter' => $twitter];
        }

        if(!$user->update()){

            return response(['error'=> true, 'message' => 'Error Saving user details!']);
        }
        return response($res);
    }

    public function getHistory()
    {
        $user = Auth::user();

        $quotations = DB::table('quotations')->where([
            ['user_id', '=', $user->id],
            ['quotrdelete', '=', false],
        ])->get();

        $sentquotations = DB::table('sent_quotations')->where([
            ['user_id', '=', $user->id],
            ['quotrdelete', '=', false],
        ])->get();

        foreach ($quotations as $quotation){
            $quotation->from_id = $quotation->from;
            $quotation->from = User::find($quotation->from)->name;
        }
//
        foreach ($sentquotations as $sentquotation){
            $sentquotation->madeto_id = $sentquotation->madeto;
            $sentquotation->madeto =  User::find($sentquotation->madeto)->name;
        }

        return response(['quotations'=>$quotations, 'sentquotations'=> $sentquotations]);
//        return response(['error'=> false,'quotations'=>$quotations, 'requests'=>$userrequests], 200);
    }

    public function getCheckout(Request $request)
    {
        $cart = $request['cart'];
        if (!$cart)
            return \response(['error'=>true, 'message'=>'Cart is Empty! Add Items Before Checking Out!']);
//        Mail::to($request->user())->send(new MailQuote($cart));

        $products = $cart['products'];
        $services = $cart['services'];

        if (!empty($products)) {
            foreach ($products as $key => $value) {
                if ($key != 'totalProducts') {
                    $product = Products::find($key);

                    // Check if ordered qty is nt greater than in store
                    if ($value['qty'] > $product->qty) {
                        return response(['error' => true, 'message' => "We Don't Have Enough of $product->name , Only $product->qty Remaining! Edit item before Checking out"], 422);
                    }
                }
            }
        }

        $quotation = new Quotation();

        $quotation->totalproducts = $cart['products']['totalProducts'];
        $quotation->totalservices = $cart['services']['totalServices'];
        $quotation->totalprice = $cart['totalPrice'];
        $quotation->quotrdelete = true;
        $quotation->quoteedelete = true;
        $quotation->madeto = $cart['quoteTo'];
//        dd($quotation);

        $quotation = $request->user()->quotation()->save($quotation);

        if (!empty($products)) {
            foreach ($products as $key => $value) {
                if ($key != 'totalProducts') {
                    $orderedProduct = new OrderedProducts();

                    $orderedProduct->product_id = $key;
                    $orderedProduct->quotation_id = $quotation->id;
                    $orderedProduct->qty = $value['qty'];

                    $orderedProduct->save();
                }
            }
        }

        if (!empty($services)) {
            foreach ($services as $key => $value) {
                if ($key != 'totalServices') {
                    $orderedService = new OrderedServices();

                    $orderedService->service_id = $key;
                    $orderedService->quotation_id = $quotation->id;
                    $orderedService->qty = $value['qty'];

                    $orderedService->save();
                }
            }
        }
        return response(['error'=> false, 'message' => 'Thank You for Using QUOTR! The Quotation Has Been Forwarded To Your Email!'], 200);
//
//          return redirect('quotation')->withErrors(['error'=>'Could NOT send Email! Please Retry']);
    }

    public function deleteProduct($id){
        $product = Products::find($id);
        if ($product) {
            if (Auth::user() != $product->user) {
                return response(['error' => true, 'message' => 'You Don\'t Have Permission To Perform this Action!']);
            }
            $product->delete();
            return response(['error' => false, 'message' => 'Product Successfully Deleted!'], 200);
        }
        return response(['error' => true, 'message' => 'Product Not Found!']);

    }

    public function deleteService($id)
    {
        $service = Services::find($id);
        if ($service) {
            if (Auth::user() != $service->user) {
                return response(['error' => true, 'message' => 'You Don\'t Have Permission To Perform this Action!']);
            }
            $service->delete();
            return response(['error' => false, 'message' => 'Service Successfully Deleted!'], 200);
        }
        return response(['error' => true, 'message' => 'Service Not Found!']);
    }

    public function deleteQuotation(Request $request, $id)
    {
        // Boolean : false dnt display qout
        $quotation = Quotation::find($id);

        if ($quotation){
            $quotation->quotrdelete = false;

            if($quotation->update()){
                return response(['error' => false, 'message'=>'Item Successfully Deleted!!'],200);
            }
            return response(['error' => true, 'message'=>"Couldn't Delete Item Plese, Try Again!"], 422);
        }
        return response(['error' => true, 'error'=>'Item Not Found'], 403);
    }

    public function deleteSentQuotation(Request $request, $id)
    {
        // Boolean : false dnt display qout
        $sent_quotation = SentQuotation::find($id);
        if($sent_quotation) {
            $sent_quotation->quotrdelete = true;

            if ($sent_quotation->update()) {
                return response(['error' => false, 'message' => 'Item Successfully Deleted!!'], 200);
            }
            return response(['error' => true, 'message' => "Couldn't Delete Item Plese, Try Again!"], 422);
        }
        return response(['error' => true, 'error'=>'Item Not Found'], 403);
    }

    public function sendQuotationComplete(Request $request)
    {
        if ($request['cart']) {
            $cart = $request['cart'];
            $to =  $cart['to'];

//            Mail::to($request->user())->send(new MailQuote($cart));

            $products = $cart['products'];
            $services = $cart['services'];

            if (!empty($products)) {
                foreach ($products as $key => $value) {
                    if ($key != 'totalProducts') {
                        $product = Products::find($key);

                        // Check if ordered qty is nt greater than in store
                        if ($value['qty'] > $product->qty) {
                            return response(['error' => true, 'error' => "We Don't Have Enough of $product->name , Only $product->qty Remaining! Edit item before Checking out"]);
                        }
                    }
                }
            }

            $quotation = new SentQuotation();

            $quotation->totalproducts = $cart['products']['totalProducts'];
            $quotation->totalservices = $cart['services']['totalServices'];
            $quotation->totalprice = $cart['totalPrice'];
            $quotation->quotrdelete = false;
            $quotation->quoteedelete = false;
            $quotation->madeto = $cart['to']->id;
//            dd($quotation);

            $quotation = $request->user()->quotation()->save($quotation);

            if (!empty($products)) {
                foreach ($products as $key => $value) {
                    if ($key != 'totalProducts') {
                        $orderedProduct = new OrderedProducts();

                        $orderedProduct->product_id = $key;
                        $orderedProduct->quotation_id = $quotation->id;
                        $orderedProduct->qty = $value['qty'];

                        $orderedProduct->save();
                    }
                }
            }

            if (!empty($services)) {
                foreach ($services as $key => $value) {
                    if ($key != 'totalServices') {
                        $orderedService = new OrderedServices();

                        $orderedService->service_id = $key;
                        $orderedService->quotation_id = $quotation->id;
                        $orderedService->qty = $value['qty'];

                        $orderedService->save();
                    }
                }
            }
            $request->session()->forget('send-cart');

            return response(['error' => false, 'mesage' => 'The Quotation Has Been Forwarded To ' . $to->name . '\'s Email!']);

        } else {
            return response(['error' => true, 'message' => 'Populate your cart first to get Quotation!']);
        }
    }

    // Preview for mobile
    public function mobileSendQuotationPreview($sendcart)
    {
        if ($sendcart) {
            $cart = $sendcart;

            // Restructuring the cart Object
            $cart['quoteTo'] = $quoteTo->id;

            $cart['to'] = $quoteTo;
            $cart['from'] = Auth::user();

            Session::put('mobile_cart', $cart);
            return view('mobile.preview')->with('cart', $cart);
        }
        Session::flash('error', "An error occurred. Please Retry!");

        return view('mobile.error');
    }

    public function mobileGetQuotationPreview($cart)
    {
        if ($cart) {
            $cart['to'] = Auth::user();
            $cart['from'] = User::find($cart['quoteTo'])->first();

            session(['mail-cart'=>$cart]);

            Session::put('mobile_cart', $cart);
            return view('mobile.preview')->with('cart', $cart);
        }
        Session::flash('error', "An error occurred. Please Retry!");

        return view('mobile.error');
    }


    // Checkout mobile-preview
    public function mobileQuotationComplete($cart)
    {
        if($cart) {
            $cart['quoteTo'] = User::find($cart['quoteTo']);
            if ($cart['to']) {
                $quoteTo = $cart['to'];
            }else {
                $quoteTo = User::find($cart['quoteTo']);
            }

            Mail::to($quoteTo)->send(new MailQuote($cart));

            $products = $cart['products'];
            $services = $cart['services'];

            if (!empty($products)) {
                foreach ($products as $key => $value) {
                    if ($key != 'totalProducts') {
                        $product = Products::find($key);

                        // Check if ordered qty is nt greater than in store
                        if ($value['qty'] > $product->qty) {
                            return redirect()->back()->with(['info' => "We Don't Have Enough of $product->name , Only $product->qty Remaining! Edit item before Checking out"]);
                        }
                    }
                }
            }
            if(!$cart['quoteTo']){
                $cart['quoteTo'] = $cart['to']->id;
            }
            // $cart['quoteTo'] = $cart['quoteTo'];

            $quotation = new Quotation();

            $quotation->totalproducts = $cart['products']['totalProducts'];
            $quotation->totalservices = $cart['services']['totalServices'];
            $quotation->totalprice = $cart['totalPrice'];
            $quotation->quotrdelete = false;
            $quotation->quoteedelete = false;
            $quotation->from = $cart['quoteTo'];

            $quotation = $request->user()->quotation()->save($quotation);

            if (!empty($products)) {
                foreach ($products as $key => $value) {
                    if ($key != 'totalProducts') {
                        $orderedProduct = new OrderedProducts();

                        $orderedProduct->product_id = $key;
                        $orderedProduct->quotation_id = $quotation->id;
                        $orderedProduct->qty = $value['qty'];

                        $orderedProduct->save();
                    }
                }
            }

            if (!empty($services)) {
                foreach ($services as $key => $value) {
                    if ($key != 'totalServices') {
                        $orderedService = new OrderedServices();

                        $orderedService->service_id = $key;
                        $orderedService->quotation_id = $quotation->id;
                        $orderedService->qty = $value['qty'];

                        $orderedService->save();
                    }
                }
            }
            $request->session()->flush();
            // $request->session()->forget('send-cart');

            Session::flash('success', "Quotation was been Successfully!");
            return view('mobile.success');
        }
        else{
            Session::flash('error', "An error occurred. Please Retry!");
            return view('mobile.error');
        }
    }
}
