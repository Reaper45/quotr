<?php

namespace App\Http\Controllers;

use App\Mail\MailQuote;
use App\OrderedProducts;
use App\OrderedServices;
use App\Products;
use App\Quotation;
use App\SentQuotation;
use App\Services;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class QuotationController extends Controller
{

    public function deleteQuotation(Request $request)
    {
        if( Auth::guest() ){
            return redirect('/login');
        }
        // Boolean : false dnt display qout
        $quotation = Quotation::find($request['id']);

        if ($quotation){
            $quotation->quotrdelete = true;

            if($quotation->update()){
                $msg = ['message'=>'Item Successfully Deleted!!'];
                return response($msg,200);
            }
            $msg = ['message'=>"Couldn't Delete Item Plese, Try Again!"];
            return response($msg, 422);
        }
        return response(['error'=>'Item Not Found'], 403);
    }

    public function editCart()
    {
        if( Auth::guest() ){
            return redirect('/login');
        }

        return redirect()->back()->with(['success'=> 'Thank You for Using QUOTR! The Quotation Has Been Forwarded To Your Email!']);
    }

    public function emptyCart(Request $request)
    {
        if( Auth::guest() ){
            return redirect('/login');
        }

        if (!Session::has('cart')) {
            return redirect()->back()->withErrors(['error' => 'Your Quote Cart is Already Empty. Populate your cart to get Quotation!']);
        }
        $request->session()->forget('cart');
        return redirect('categories')->with(['success' => 'Items Removed From Quote Cart! Populate your cart to get Quotation!']);
    }

    public function getQuotationPreview(Request $request)
    {
        // dd(Session::get('cart'));
        if (!Session::has('cart')){
            return redirect('categories')->withErrors(['error'=>'Your Quote Cart Is Empty. Add Products/Services to get Quotation!!']);
        }
        return view('get-quot-preview');
    }

    public function sendQuotationPreview(Request $request, $name)
    {
        $send_cart = Session::get('send-cart');
        $send_cart['to'] = User::where('name', '=', $name)->first();

        $request->session()->put(['send-cart'=>$send_cart]);
        // dd(Session::get('send-cart')['to']);
        Session::flash('info','Sending Quotation to '.$name);
        return view('send-quot-preview');
    }

    public function emptysendCart(Request $request)
    {
        if( Auth::guest() ){
            return redirect('/login');
        }

        if (!Session::has('send-cart')) {
            return redirect()->back()->withErrors(['error' => 'Your Quote Cart is Already Empty. Populate your cart to get Quotation!']);
        }
        $request->session()->forget('send-cart');
        return redirect('user/account')->with(['success' => 'Items Removed From Quote Cart! Populate your cart to get Quotation!']);
    }

    public function deleteSentQuotation(Request $request)
    {
        if( Auth::guest() ){
            return redirect('/login');
        }
        // Boolean : false dnt display qout
        $sent_quotation = SentQuotation::find($request['id']);
        if($sent_quotation) {
            $sent_quotation->quotrdelete = true;

            if ($sent_quotation->update()) {
                $msg = ['message' => 'Item Successfully Deleted!!'];
                return response($msg, 200);
            }
            $msg = ['message' => "Couldn't Delete Item Plese, Try Again!"];
            return response($msg, 422);
        }
        return response(['error'=>'Item Not Found'], 403);
    }

