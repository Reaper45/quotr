<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Products;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public $products = null;
    public $services = null;
    public $totalQty = 0;
    public $totalPrice = 0;
    public $quoteTo;

    public $send_products = null;
    public $send_services = null;
    public $send_totalQty = 0;
    public $send_totalPrice = 0;
    public $send_quoteTo;


    public function postProduct(Request $request)
    {
        $fileName = 'null';

        $validator = Validator::make($request->all(), [
            'name'        => 'required',
            'description' => 'required|max:120',
            'category'    => 'required',
            'image'       => 'required',
            'price'       => 'required'
        ]);

        if ($validator->fails()){
            $error = $validator->errors();

            return response(['errors' => $error], 403);
        }

        if (Input::file('image')->isValid()) {
            $file = Input::file('image');
            $extension = Input::file('image')->getClientOriginalExtension();

            $fileName = uniqid() . '.' . $extension;

            $content = File::get($file);

            if (Storage::disk('productImages')->put($fileName, $content)) {

                $product = new Products();

                $product->name = $request['name'];
                $product->description = $request['description'];
                $product->image = $fileName;
                $product->price = $request['price'];
                $product->qty = $request['qty'];

                if ($request->user()->product()->save($product)){
                    return response($product, 200);
                }
            }
        }
    }

    public function fetchProductsFromDB($product_id = null)
    {
        if(isset($product_id)){
            $product = Products::where('id', '=', $product_id)->get()->first();

            return $product;
        }
        else{
            $allProduct = Products::all();

            return $allProduct;
        }
    }

    public function fetchCompanyProducts($user_id)
    {
        $companyProducts = Products::orderby('created_at', 'desc')->where('user_id', '=', $user_id)->get();

        return $companyProducts;
    }

    public function getProductDetails($user_id, $product_id)
    {
        $product = $this->fetchProductsFromDB($product_id);
        $products = Products::where('user_id', '=',$user_id)->where('id', '!=', $product_id)->paginate(12);

        return view('addtocart-product',['product'=>$product, 'products'=>$products]);

    }

    public function  getProductImage($filename){
        $file = Storage::disk('productImages')->get($filename);
        return new Response($file, 200);
    }

    public function deleteProduct($id){
        $product = Products::find($id);

        if(Auth::user() != $product->user)
        {
            return redirect()->back();
        }
        $product->delete();
        return redirect()->back()->with(['success'=>'Product Successfully Deleted!']);
    }

    public function updateProduct(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'price' => 'required',
            'qty' => 'required',
            'description' => 'required|max:120'
        ]);

        if($validator->fails()){
            return response(['errors'=>$validator->errors()],403);
        }

        $product = Products::find($request['id']);

        $product->name = $request['name'];
        $product->price = $request['price'];
        $product->qty = $request['qty'];
        $product->description = $request['description'];

        if($request['image'] != null) {
            if (Input::file('image')->isValid()) {
                $file = Input::file('image');
                $extension = Input::file('image')->getClientOriginalExtension();

                $fileName = uniqid() . '.' . $extension;

                $content = File::get($file);

                if (Storage::disk('productImages')->put($fileName, $content)) {
                    $product->image = $fileName;
                }
            }
        }
        if ($product->update())
            return response(['message'=>'Product Successfully Updated!'], 200);

        return response(['error'=> 'Could NOT save Changes. Please Retry!'],422);
    }

    public function addToCart(Request $request, $id)
    {
        $qty = $request['qty'];

        $product = Products::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;

        $user = $product->user->id;

        if($oldCart){
            if ( $oldCart['quoteTo'] != $user ){
                return redirect()->route('get.users', $oldCart['quoteTo'] )->with(['info'=> 'Items MUST be from one Business! Empty Quote Cart first if you wish to Change Business']);
            }
            $this->products = $oldCart['products'];
            $this->totalQty = $oldCart['totalQty'];
            $this->totalPrice = $oldCart['totalPrice'];
            $this->services = $oldCart['services'];
            $this->quoteTo = $oldCart['quoteTo'];
        }

        $this->quoteTo = $user;

        // Define item to be stored
        $storedItem = ['qty' => 0, 'price' => $product->price, 'product'=>$product];

        // Check if any products have been set before
        if($this->products){
            // Check if item already exists in cart
            if(array_key_exists($id, $this->products)){
                // If true, update quantity,sum-price
                $storedItem = $this->products[$id];

                $initialQty = $this->products[$id]['qty'];
                $storedItem['qty'] = $initialQty != $qty ? $qty : $initialQty;

                //Just setting a success message
                $message = 'Item Successfully Updated!';
            }else{
                // If false, set new properties
                $storedItem['qty'] = $qty;
                $this->totalQty++;

                //Just setting a success message
                $message = 'Item Successfully Added to Quote Cart!!';
            }
            // Finally reset the total price of all items to without the current item
            $this->totalPrice -= $this->products['totalProducts'];
        }
        else{
            // If its the first product, set qty and total qty of items in cart
            $storedItem['qty'] = $qty;
            $this->totalQty++;

            //Just setting a success message
            $message = 'Item Successfully Added to Quote Cart!!';
        }

        //Calculate total price
        $storedItem['price'] = $product->price * $storedItem['qty'];

        // Save/Update product as assoc array with id as key
        $this->products[$id] = $storedItem;
        $this->products['totalProducts'] = 0 ;

        // Add all product in cart and store total price
        foreach ($this->products as $item){
            $this->products['totalProducts'] += $item['price'];
        }

        $this->totalPrice += $this->products['totalProducts'];

        // Initialize the cart with all properties set
        $cart = ['products'=>$this->products, 'services'=>$this->services, 'totalQty'=>$this->totalQty, 'totalPrice'=>$this->totalPrice, 'quoteTo'=>$this->quoteTo];

        // Grab request session and insert cart
        $request->session()->put('cart', $cart);
//        dd(Session::get('cart')); Its just a debugging stuff
        Session::flash('success',  $message);

        // Redirect where we are from
        return redirect()->back();
    }

    public function removeFromCart(Request $request, $id)
    {
        if(!Session::has('cart')){
            return redirect('/categories')->withErrors(['error'=>'Your Cart Is Empty!']);
        }
        $oldCart = Session::get('cart');


        if($oldCart){
            $this->services = $oldCart['services'];
            $this->totalQty = $oldCart['totalQty'];
            $this->totalPrice = $oldCart['totalPrice'];
            $this->products = $oldCart['products'];
            $this->quoteTo = $oldCart['quoteTo'];
        }

        $product = $this->products[$id];
        if(!$product){
            return redirect()->back()->withErrors(['error'=>'Item Does NOT exist in Your Quote Cart!']);
        }
        $price = $product['price'];

        $this->products['totalProducts'] -= $price;
        $this->totalQty --;
        $this->totalPrice -= $price;

        unset($this->products[$id]);

        $cart = ['services'=>$this->services,
            'products'=>$this->products,
            'totalQty'=>$this->totalQty,
            'totalPrice'=>$this->totalPrice,
            'quoteTo'=> $this->quoteTo];

        $request->session()->put('cart', $cart);
        return redirect()->back()->with(['success'=> 'Item Removed From Quote Cart!']);
    }

    public function addToQuotation(Request $request, $id)
    {
        $qty = $request['qty'];

        $product = Products::find($id);
        $oldCart = Session::has('send-cart') ? Session::get('send-cart') : null;

        $user = $product->user->id;

        if($oldCart){

            $this->send_products = $oldCart['products'];
            $this->send_totalQty = $oldCart['totalQty'];
            $this->send_totalPrice = $oldCart['totalPrice'];
            $this->send_services = $oldCart['services'];
            $this->send_quoteTo = $oldCart['quoteTo'];
        }

        $storedItem = ['qty' => 0, 'price' => $product->price, 'product'=>$product];

        if($this->send_products){
            if(array_key_exists($id, $this->send_products)){
                $storedItem = $this->send_products[$id];

                $initialQty = $this->send_products[$id]['qty'];
                $storedItem['qty'] = $initialQty != $qty ? $qty : $initialQty;

                $message = 'Item Successfully Updated!';
            }else{
                $storedItem['qty'] = $qty;
                $this->send_totalQty++;

                $message = 'Item Successfully Added to Quote Cart!!';
            }
            $this->send_totalPrice -= $this->send_products['totalProducts'];
        }
        else{
            $storedItem['qty'] = $qty;
            $this->send_totalQty++;

            $message = 'Item Successfully Added to Quote Cart!!';
        }

        $storedItem['price'] = $product->price * $storedItem['qty'];

        $this->send_products[$id] = $storedItem;
        $this->send_products['totalProducts'] = 0 ;

        foreach ($this->send_products as $item){
            $this->send_products['totalProducts'] += $item['price'];
        }

        $this->send_totalPrice += $this->send_products['totalProducts'];

        $cart = ['products'=>$this->send_products,
            'services'=>$this->send_services,
            'totalQty'=>$this->send_totalQty,
            'totalPrice'=>$this->send_totalPrice,
            'quoteTo'=>null];

        $request->session()->put('send-cart', $cart);
//        dd(Session::get('cart')); Its just a debugging stuff
        Session::flash('success',  $message);

        return redirect()->back();
    }

    public function sendQuotationProducts(Request $request)
    {
        if(Auth::user()) {
            $products = Products::orderby('created_at', 'desc')->where('user_id', '=', Auth::user()->id)->paginate(12);

            return view('user-products')->with(['products' => $products]);
        }
    }

    public function removeFromSendCart(Request $request, $id)
    {
        if(!Session::has('send-cart')){
            return redirect('user/send-quotation')->withErrors(['error'=>'Your Quote Cart Is Empty!']);
        }
        $oldCart = Session::get('send-cart');

        if($oldCart){
            $this->services = $oldCart['services'];
            $this->totalQty = $oldCart['totalQty'];
            $this->totalPrice = $oldCart['totalPrice'];
            $this->products = $oldCart['products'];
            $this->quoteTo = $oldCart['quoteTo'];
        }

        $product = $this->products[$id];
        if(!$product){
            return redirect()->back()->withErrors(['error'=>'Item Does NOT exist in Your Quote Cart!']);
        }
        $price = $product['price'];

        $this->products['totalProducts'] -= $price;
        $this->totalQty --;
        $this->totalPrice -= $price;

        unset($this->products[$id]);

        $cart = ['services'=>$this->services,
            'products'=>$this->products,
            'totalQty'=>$this->totalQty,
            'totalPrice'=>$this->totalPrice,
            'quoteTo'=> $this->quoteTo];

        $request->session()->put('send-cart', $cart);
        return redirect()->back()->with(['success'=> 'Item Removed From Quote Cart!']);
    }


}
