<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsRegComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::user()){
            if( Auth::user()->logo == null ||
                Auth::user()->phone == null ||
                Auth::user()->category_id == null ||
                Auth::user()->bio == null ||
                Auth::user()->location == null){

                return redirect()->route('user.editprofile')->with(['info'=>'Complete Your Profile To Send and Get Quotation!']);
            }
            return $next($request);
        }
        return redirect('/login');
    }
}
