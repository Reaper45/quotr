<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable = ['totalproducts', 'totalservices', 'totalprice'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function orderedProducts()
    {
        $this->hasMany('App\OrderedProducts');
    }

    public function orderedServices()
    {
        $this->hasMany('App\OrderedServices');
    }
}
