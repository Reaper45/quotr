/**
 * Created by reaper45 on 3/27/17.
 */

var postUrl = postProductUrl;
var prodType = 'product';

$('#myTabs a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
})

$('#addnew-btn').on('click', function (event) {
    $('.list-group-item').removeClass('active')
    $('#edit-tab-item').addClass('active')
})

$('.list-group-item').on('click', function (event) {
    $('.list-group-item').removeClass('active')
    $(this).addClass('active')
})

//noinspection JSUnresolvedFunction
$('#itemcategory').change(function () {
    prodType = document.getElementById("itemcategory").value;
    if (prodType == 'product'){
        postUrl = postProductUrl;
        $('#qtyAvailable').css('display', 'block')
        $('#priceCol').addClass('col-md-6')
    }
    else if (prodType == 'service'){
        postUrl = postServiceUrl;
        $('#qtyAvailable').css('display', 'none')
        $('#priceCol').removeClass('col-md-6')
    }
})

// Generate image preview before upload
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#profilepic').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#file").change(function () {
    readURL(this);
});

// Get the selected image profile and Cha button text ux
var inputs = document.querySelectorAll( '.inputfile' );
Array.prototype.forEach.call( inputs, function( input ){
    var label	 = input.nextElementSibling,
        labelVal = label.innerHTML;

    input.addEventListener( 'change', function(e){
        var	fileName = e.target.value.split( '\\' ).pop();

        if( fileName )
            label.querySelector( 'span' ).innerHTML = fileName;
        else
            label.innerHTML = labelVal;
    });
});

// Editing A Product
$('.edit-product').on('click', function (e) {
    e.preventDefault()
    parentDiv = $(this).parent().parent();
    id = parentDiv.children('input:nth-child(1)').val();
    name = parentDiv.children('input:nth-child(2)').val();
    price = parentDiv.children('input:nth-child(3)').val();
    desc = parentDiv.children('input:nth-child(4)').val();
    qty = parentDiv.children('input:nth-child(5)').val();

    var getmodalform = $('#editProduct').children().children().children().next().children()

    getmodalform.children('input:nth-child(2)').val(id)
    getmodalform.children('div:nth-child(3)').children().next().val(name)
    getmodalform.children('div:nth-child(4)').children().next().children().next().val(price)
    getmodalform.children('div:nth-child(5)').children().next().children().val(qty)
    getmodalform.children('div:nth-child(7)').children().next().val(desc)

});

// Editing A Service
$('.edit-service').on('click', function (e) {
    e.preventDefault()
    parentDiv = $(this).parent().parent();
    id = parentDiv.children('input:nth-child(1)').val();
    name = parentDiv.children('input:nth-child(2)').val();
    price = parentDiv.children('input:nth-child(3)').val();
    desc = parentDiv.children('input:nth-child(4)').val();

    var getmodalform = $('#editService').children().children().children().next().children()

    getmodalform.children('input:nth-child(2)').val(id)
    getmodalform.children('div:nth-child(3)').children().next().val(name)
    getmodalform.children('div:nth-child(4)').children().next().children().next().val(price)
    getmodalform.children('div:nth-child(6)').children().next().val(desc)

});

function ajaxRequest(formdata, title, text, url, w1, w2){

    swal({
        title: title,
        text: text,
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function () {
        setTimeout(function () {
            $.ajax({
                method: 'POST',
                url: url,
                data: formdata,
                cache: false,
                contentType: false,
                processData: false,
                statusCode: {
                    403: function (err) {
                        // console.log(err.responseJSON.errors)
                        swal("Error", "Ensure all Fields are Correct :)", "error")
                    },
                    422: function (err) {
                        console.log(err.message)
                        swal("Error", +err.message+ " :)", "error");

                    }
                }
            }).done(function (msg) {
                // w2 = msg.message;
                console.log(msg)
                swal(w1, w2, 'success');
                window.location.reload();
            })
        }, 2000);
    });
}

/**
 * The Below cde implements the above function, JUst trying reduce redundancy here
 */

// Update user profile
$('#updateProfileForm').submit(function (event) {
    event.preventDefault()
    var formdata = new FormData(this)

    var title = "Save account updates?";
    var text = "Submit to commit changes";
    var url = saveAccountChanges

    var w1 = "Updated!";
    var w2 = "Your account details are upto date!";

    ajaxRequest(formdata, title, text, url, w1, w2);
});

//Post a new Product/service
$('#newpostForm').submit(function (event) {
    event.preventDefault()
    var formdata = new FormData(this)

    var title = "Save Post?";
    var text = "Submit to commit changes";

    var w1 = "Saved!";
    var w2 = "You posted a new "+prodType+"!";

    ajaxRequest(formdata, title, text, postUrl, w1, w2);
});

// Delete qoutation From profile
$('#delete-quotation').submit(function (event) {
    event.preventDefault()
    var formdata = new FormData(this)

    var title = "Delete Quotation?";
    var text = "Submit to commit changes";

    var w1 = "Delete!";
    var w2 = "You've Successfully Deleted The Quotation!";

    ajaxRequest(formdata, title, text, deleteQuotation, w1, w2);
});

// Delete qoutation-sent From profile
$('#delete-request').submit(function (event) {
    event.preventDefault()
    var formdata = new FormData(this)

    var title = "Delete Sent Quotation?";
    var text = "Submit to commit changes";

    var w1 = "Delete!";
    var w2 = "You've Successfully Deleted The Quotation!";

    ajaxRequest(formdata, title, text, deleteSentQuotation, w1, w2);
});

// Submit Product changes
$('#editProductForm').submit(function (event) {
    event.preventDefault()
    var formdata = new FormData(this)
    // console.log(formdata)

    var title = "Save Changes?";
    var text = "Submit to commit changes";

    var w1 = "Saved!";
    var w2 = "You've updated your Products!";

    ajaxRequest(formdata, title, text, editProductUrl, w1, w2);
});

// Submit Product changes
$('#editServiceForm').submit(function (event) {
    event.preventDefault()
    var formdata = new FormData(this)

    var title = "Save Changes?";
    var text = "Submit to commit changes";

    var w1 = "Saved!";
    var w2 = "You've updated your Services!";

    ajaxRequest(formdata, title, text, editServiceUrl, w1, w2);
});

// Edit Item in cart
var cart = {};
$('.sendquotr').change( function (event) {
    id = $(this).val()
    if(this.checked){
        $(this).next().show()
        cart[id] = 'qty'
    }
    else{
        $(this).next().hide()
        delete cart[id]
    }
})

$('#btnsendquotr').on('click', function () {
    if(Object.keys(cart).length > 0){
        var title = "Send Quotation?";
        var text = "Submit to commit changes";

        var w1 = "Sent!";
        var w2 = "Quotation Successfully Sent!";

        ajaxRequest(cart, title, text, sendQuotationUrl, w1, w2);
    }else
        swal("Error", " Working", "error")
})